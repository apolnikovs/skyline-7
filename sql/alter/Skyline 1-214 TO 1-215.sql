# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.214');

# ---------------------------------------------------------------------- #
# Modify Table service_provider         	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider`
	ADD COLUMN `DpdFtpServer` VARCHAR(50) NULL DEFAULT NULL AFTER `ViamenteKey2MaxWayPoints`,
	ADD COLUMN `DpdFtpUser` VARCHAR(30) NULL DEFAULT NULL AFTER `DpdFtpServer`,
	ADD COLUMN `DpdFtpPassword` VARCHAR(30) NULL DEFAULT NULL AFTER `DpdFtpUser`,
	ADD COLUMN `DpdFilePrefix` VARCHAR(30) NULL DEFAULT NULL AFTER `DpdFtpPassword`,
	ADD COLUMN `DpdFileCount` INT(11) NOT NULL DEFAULT '0' AFTER `DpdFilePrefix`;

# ---------------------------------------------------------------------- #
# Add Table national_bank_holiday                                        #
# ---------------------------------------------------------------------- #
CREATE TABLE national_bank_holiday 
									(
										NationalBankHolidayID INT(10) NOT NULL AUTO_INCREMENT, 
										HolidayDate DATE NOT NULL, 
										HolidayName VARCHAR(50) NOT NULL, 
										CountryID INT NOT NULL DEFAULT '1', 
										PRIMARY KEY (NationalBankHolidayID) 
									) 	COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

									
# ---------------------------------------------------------------------- #
# Add Table service_provider_bank_holidays                               #
# ---------------------------------------------------------------------- #
CREATE TABLE service_provider_bank_holidays 
											( 
												ServiceProviderBankHolidaysID INT(10) NULL AUTO_INCREMENT, 
												NationalBankHolidayID INT(10) NOT NULL DEFAULT '0', 
												ServiceProviderID INT(10) NOT NULL DEFAULT '0', 
												Status ENUM('Open','Closed') NOT NULL DEFAULT 'Closed', 
												PRIMARY KEY (ServiceProviderBankHolidaysID) 
											) 	COLLATE='latin1_swedish_ci' ENGINE=InnoDB;
											
											
#------------------------------------------------------------------------#
# Update System Data													 #
#------------------------------------------------------------------------#
INSERT IGNORE INTO national_bank_holiday (NationalBankHolidayID, HolidayDate, HolidayName, CountryID) 
		VALUES (1, '2013-01-01', 'New Year\'s Day', 1), 
		(2, '2013-03-29', 'Good Friday', 1), 
		(3, '2013-04-01', 'Easter Monday', 1), 
		(4, '2013-05-06', 'Early May bank holiday', 1), 
		(5, '2013-05-27', 'Spring bank holiday', 1), 
		(6, '2013-08-26', 'Summer Bank Holliday', 1), 
		(7, '2013-12-25', 'Christmas Day', 1), 
		(8, '2013-12-26', 'Boxing Day', 1); 
											

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.215');
