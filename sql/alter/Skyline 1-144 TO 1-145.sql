# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.144');


# ---------------------------------------------------------------------- #
# Modify table "job"                                                 #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `SamsungSubServiceType` VARCHAR(2) NULL DEFAULT NULL AFTER `CourierID`;
ALTER TABLE `job` ADD COLUMN `SamsungRefNo` VARCHAR(15) NULL DEFAULT NULL AFTER `SamsungSubServiceType`;
ALTER TABLE `job` ADD COLUMN `ConsumerType` VARCHAR(30) NULL DEFAULT NULL AFTER `SamsungRefNo`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.145');