# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.204');

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` ADD COLUMN `TempSpecified` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `ConfirmedByUser`; 

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD COLUMN `ViamenteRunType` ENUM('AllEngineers','CurrentEngineer') NOT NULL DEFAULT 'AllEngineers' AFTER `AdminSupervisorEmail`;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.205');
