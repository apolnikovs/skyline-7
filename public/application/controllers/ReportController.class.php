<?php

require_once('CustomSmartyController.class.php');

class ReportController extends CustomSmartyController {
    
    
    
    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = "en";
    public $smarty;
    public $page;
    public $statuses =[
	0 => ["Name" => "Active", "Code" => "Active"],
	1 => ["Name" => "In-active", "Code" => "In-active"]
    ];
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
        $this->config = $this->readConfig('application.ini');
        
        $this->session = $this->loadModel('Session'); 
        
        $this->messages = $this->loadModel('Messages'); 
        
	if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
	    $this->smarty->assign('loggedin_user', $this->user);

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            
            if($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;

        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', ''); 
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect("index", null, null);
            
        } 
        
        if ($topLogoBrandID) {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign("_brandLogo", (isset($topBrandLogo[0]["BrandLogo"])) ? $topBrandLogo[0]["BrandLogo"] : "");
        } else {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
	if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
	
	$user_model->initiatePermissions($this);
	
    }
       
    
    
    public function indexAction() {
        
	$this->page = $this->messages->getPage('ReportGenerator', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('ReportGenerator');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        
        $this->smarty->assign('page', $this->page);
	$this->smarty->display('ReportGenerator.tpl'); 

    }     
    
    
    
    public function jobsBookedAction() {
	
	set_time_limit(10000);
	
	require_once("libraries/PHPExcel/PHPExcel.php");
	require_once("libraries/PHPExcel/PHPExcel/Writer/Excel2007.php");
	$excel = new PHPExcel();
	
	$excel->setActiveSheetIndex(0);
	
	$report_model = $this->loadModel("Reports");
	$helper_model = $this->loadModel("Helpers");
	
	$args = ["dateFrom" => $_GET["dateFrom"], "dateTo" => $_GET["dateTo"]];
	
	$report = $report_model->getJobsBooked($args);
	
	if (count($report) == 0) {
	    
	    $excel->getActiveSheet()->SetCellValue("A1", "No jobs found.");
	    
	} else {
	
	    $header = array_keys($report[0]);

	    $colCount = count($header)-1;

	    $colNum = 0;
	    foreach($header as $name) {
		$colName = $helper_model->getExcelColName($colNum);
		$excel->getActiveSheet()->SetCellValue($colName . "1", $name);
		$colNum++;
	    }

	    //grey row, grey font
	    $greyRow = array(
		'fill' => array(
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => array('rgb'=>'DDDDDD')
		),
		'font' => array(
		    'color' => array('rgb'=>'737373')
		)
	    );

	    //grey font
	    $greyFont = array(
		'font' => array(
		    'color' => array('rgb'=>'737373')
		)
	    );

	    foreach($report as $rowNum => $job) {
		$colNum = 0;
		foreach($job as $field) {
		    $colName = $helper_model->getExcelColName($colNum);
		    $field = str_replace("=", "", $field);
		    $excel->getActiveSheet()->SetCellValue($colName . ($rowNum + 2), $field);
		    $colNum++;
		}
		//row styling
		if(($rowNum % 2)) {
		    $colName = $helper_model->getExcelColName($colCount);
		    $excel->getActiveSheet()->getStyle('A' . ($rowNum+2) . ":" . $colName . ($rowNum+2))->applyFromArray($greyRow);
		} else {
		    $colName = $helper_model->getExcelColName($colCount);
		    $excel->getActiveSheet()->getStyle('A' . ($rowNum+2) . ":" . $colName . ($rowNum+2))->applyFromArray($greyFont);
		}
	    }

	    //set auto width to columns
	    $colNum = 0;
	    foreach($header as $name) {
		$colName = $helper_model->getExcelColName($colNum);
		$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(true);
		$colNum++;
	    }

	    //header styling
	    $style_header = array(
		'fill' => array(
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => array('rgb'=>'158DC6')
		),
		'font' => array(
		    'bold' => true,
		    'color' => array('rgb'=>'FFFFFF')
		)
	    );
	    $colName = $helper_model->getExcelColName($colCount);
	    $excel->getActiveSheet()->getStyle('A1:' . $colName . '1')->applyFromArray($style_header);

	    $excel->getActiveSheet()->setAutoFilter('A1:' . $colName . "1");
	
	}
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="jobs_booked_report.xlsx"');
	header('Cache-Control: max-age=0');

	$excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	$excelWriter->save('php://output'); 
	exit();
	
    }
    
    
    
    public function samsungTransactionReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	$reportModel->samsungTransactionReport();
	
    }
    
 
    
    public function downloadReportFileAction() {
	
	ignore_user_abort(true);
	
	$data = $_REQUEST;
	
	$file = "reports/{$data["filename"]}.xlsx";
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="' . $data["filename"] . '.xlsx"');
	header('Cache-Control: max-age=0');
	header("Content-Length: " . filesize($file));
	
	readfile("reports/{$data["filename"]}.xlsx");
	
	unlink($file);
	
	exit();
	
    }
    
    
    
    public function reportProgressAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->reportProgress();
	echo($result);
	
    }
    
    
    
    public function generatorAction() {
	
	$this->page = $this->messages->getPage("generator", $this->lang);
        $this->smarty->assign("page", $this->page);
	$this->smarty->display("generator.tpl");

    }

    
    
    public function createUserReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	
	$this->page = $this->messages->getPage("generator", $this->lang);
        $this->smarty->assign("page", $this->page);
	
	$primaryTables = $reportModel->getPrimaryTables();
        $this->smarty->assign("primaryTables", $primaryTables);
	
	$this->smarty->display("createReport.tpl");

    }
    
    
    
    public function getUserReportsAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getUserReports();
	echo json_encode(["aaData" => $result]);
	
    }
    
    
    
    public function getSecondaryTablesAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getSecondaryTables($_REQUEST["tableName"]);
	echo json_encode($result);
	
    }
    
    
    
    public function getTableColumnsAction() {

	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getTableColumns($_REQUEST["tableName"]);
	echo json_encode($result);
	
    }
    
    
    
    public function serviceActionTrackingReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->serviceActionTrackingReport($_REQUEST);
	echo($result);
	
    }
    
    
    
    public function saveUserReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->saveUserReport(json_decode($_REQUEST["data"]), $_REQUEST["id"], $_REQUEST["schedule"], $_REQUEST["scheduleStatus"]);
	echo $result;
	
    }
    
    
    
    public function runUserReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	
	$id = $_REQUEST["reportID"];
	$filter = $_REQUEST["filter"];
	
	$result = $reportModel->runUserReport($id, $filter);
	
	echo(json_encode($result));
	
    }
    
    
    
    public function editUserReportAction() {
	
	$reportModel = $this->loadModel("Reports");
	
	$this->page = $this->messages->getPage("generator", $this->lang);
        $this->smarty->assign("page", $this->page);
	
	$primaryTables = $reportModel->getPrimaryTables();
        $this->smarty->assign("primaryTables", $primaryTables);
	
	if($_GET["id"]) {
	    $reportData = $reportModel->getReportData($_GET["id"]);
	    if($reportData) {
		$this->smarty->assign("reportName", $reportData["Name"]);
		$this->smarty->assign("reportDescription", $reportData["Description"]);
		$structureObj = json_decode($reportData["Structure"]);
		$this->smarty->assign("primaryTable", $structureObj->primaryTable);
		$this->smarty->assign("multiTable", $structureObj->multiTable);
		$this->smarty->assign("columns", $structureObj->columns);
		if(isset($structureObj->primaryTable) && $structureObj->primaryTable) {
		    $secondaryTables = $reportModel->getSecondaryTables($structureObj->primaryTable);
		    $this->smarty->assign("secondaryTables", $secondaryTables);
		}
		$rowCount = $reportModel->getReportRowCount($structureObj, $_GET["id"]);
		$this->smarty->assign("rowCount", $rowCount);
	    }
	    $this->smarty->assign("id", $_GET["id"]);
	    $filterColumns = $reportModel->getReportFilterColumns($structureObj);
	    $this->smarty->assign("filterColumns", $filterColumns);
	    $schedule = $reportModel->getReportSchedule($_GET["id"]);
	    $this->smarty->assign("schedule", $schedule);
	}
	
	$this->smarty->display("createReport.tpl");
	
    }
    
    
    
    public function getColumnDataAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getColumnData($_REQUEST["column"], $_REQUEST["table"], $_REQUEST["report"]);
	echo($result);
	
    }
    
    
    
    public function getReportRowCountAction() {
	
	$reportModel = $this->loadModel("Reports");
	
	$structure = isset($_REQUEST["structure"]) ? json_decode($_REQUEST["structure"]) : null;
	$id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : null;
	$filter = isset($_REQUEST["filter"]) ? json_decode($_REQUEST["filter"]) : null;
	
	$result = $reportModel->getReportRowCount($structure, $id, $filter);
	
	echo $result;
	
    }
    
    
    
    public function getReportFilterColumnsAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getReportFilterColumns(json_decode($_REQUEST["data"]));
	echo json_encode($result);
	
    }
    
    
    
    public function getDateRangeModalAction() {
	
	$reportModel = $this->loadModel("Reports");
	
	if($_REQUEST["id"]) {
	    $id = $_REQUEST["id"];
	    $reportData = $reportModel->getReportData($id);
	    if($reportData) {
		$structureObj = json_decode($reportData["Structure"]);
		$rowCount = $reportModel->getReportRowCount($structureObj, $id);
		$this->smarty->assign("rowCount", $rowCount);
	    }
	    $this->smarty->assign("id", $id);
	    $columns = $reportModel->getReportFilterColumns($structureObj);
	    $this->smarty->assign("columns", $columns);
	}
	
	$html = $this->smarty->fetch("reportDateRangeModal.tpl");
	echo $html;
	
    }
    

    
    public function deleteReportsAction() {
	
	$reportModel = $this->loadModel("Reports");
	$reportModel->deleteReports(json_decode($_REQUEST["reports"]));
	
    }
    
    
    
    public function getHelpInfoAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->getHelpInfo($_REQUEST["code"]);
	echo json_encode($result);
	
    }
    
    
    
    public function saveHelpInfoAction() {
	
	$reportModel = $this->loadModel("Reports");
	$result = $reportModel->saveHelpInfo($_REQUEST);
	
    }
    
    
    
}


?>
