<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Service Providers Page in Organisation Setup section under System Admin
 *
 * @author      Brian Etherington <b.etherington@pccsuk.com>
 * @version     1.00
 * 
 *
 * Date        Version Author                Reason
 * 23/01/2013  1.00    Brian Etherington     Initial Version

 ******************************************************************************/

class ManufacturerServiceProvider extends CustomModel {
    
    private $conn;
    private $table;    /* Used by TableFactory */

    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'],
                                      PDO::ERRMODE_SILENT );       
        
        $this->table = TableFactory::ManufacturerServiceProvider();
        
        $this->debug = false;
    }
    
    public function getManufacturersByServiceProvider( $ServiceProviderID ) {
        
        $sql = "select * from manufacturer_service_provider where ServiceProviderID=:ServiceProviderID order by ManufacturerID";
        
        $params = array( 'ServiceProviderID' => $ServiceProviderID );

        return $this->Query($this->conn, $sql, $params);
    }
    
    public function getServiceProvidersByManufacturer( $ManufacturerID ) {
        
        $sql = "select * from manufacturer_service_provider where ManufacturerID=:ManufacturerID order by ServiceProviderID";
        
        $params = array( 'ManufacturerID' => $ManufacturerID );

        return $this->Query($this->conn, $sql, $params);
    }
    
    public function fetch( $ManufacturerID, $ServiceProviderID ) {
        
        $sql = "select * from manufacturer_service_provider where ManufacturerID=:ManufacturerID and ServiceProviderID=:ServiceProviderID";
        
        $params = array( 'ManufacturerID' => $ManufacturerID,
                         'ServiceProviderID' => $ServiceProviderID );

        return $this->Query($this->conn, $sql, $params);        
    }
    
    public function update( array $Updates ) {
        
        $update_cmd = null;
        $insert_cmd = null;
        $delete_cmd = null;
        
        $errors = false;
        
        foreach($Updates as $update) {           
            
            switch ($update['mode']) {
                case Constants::UPDATE: 
                    $params = array( 'ManufacturerID'    => $update['ManufacturerID'],
                                     'ServiceProviderID' => $update['ServiceProviderID'],
                                     'Authorised'        => $update['Authorised'],
                                     'DataChecked'       => $update['DataChecked'] );
                    if ($update_cmd == null) $update_cmd = $this->table->updateCommand( $params );
                    if (!$this->Execute($this->conn, $update_cmd, $params)) $errors = true;
                    break;
                case Constants::CREATE: 
                    $params = array( 'ManufacturerID'    => $update['ManufacturerID'],
                                     'ServiceProviderID' => $update['ServiceProviderID'],
                                     'Authorised'        => $update['Authorised'],
                                     'DataChecked'       => $update['DataChecked'] );                    
                    if ($insert_cmd == null) $insert_cmd = $this->table->insertCommand( $params );
                    if (!$this->Execute($this->conn, $insert_cmd, $params)) $errors = true;
                    break;
                case Constants::DELETE: 
                    $params = array( 'ManufacturerID'    => $update['ManufacturerID'],
                                     'ServiceProviderID' => $update['ServiceProviderID']  );                    
                    if ($delete_cmd == null) $delete_cmd = $this->table->deleteCommand();
                    if(!$this->Execute($this->conn, $delete_cmd, $params)) $errors = true;
                    break;
                case Constants::NONE:
                    break;
                default:
                    // this is major programming error - bale out.
                    throw new Exception('Unrecognised Update Mode argument: '.$update['mode']);
            }
            
        }
        
        return $errors;
    }
}

?>
