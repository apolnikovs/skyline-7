<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Manufacturers Page in Product Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class Manufacturers extends CustomModel {
    
    private $conn;
    
    private $table                     = "manufacturer";
    private $table_network_manufacturer= "network_manufacturer";
    private $table_network             = "network";
    private $dbTableColumns            = array('ManufacturerID', 'ManufacturerName', 'Acronym', 'Status');
    private $dbTablesColumns           = array('ManufacturerID', 'ManufacturerName', 'Acronym', 'Status', array('ManufacturerID', 'ManufacturerID AS Assigned'));
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn= $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        $this->comonFields=
        [
                                "ManufacturerName", 
                                "Acronym",
				"Status", 
				"AuthorisationRequired",
				"AuthorisationManager",
				"AuthorisationManagerEmail",
                                "AuthReqChangedDate",
				
                                "AccountNo",
                                "Postcode",
                                "BuildingNameNumber",
				"Street",
				"LocalArea",
				"TownCity",
				"CountryID",
				"TelNoSwitchboard",
				"TelNoSpares",
				"TelNoWarranty",
				"TelNoTechnical",
				"FaxNo",
				"EmailAddress",
				"WarrantyEmailAddress",
				"SparesEmailAddress",
				"ContactName",
				
				"StartClaimNo",
				"VATNo",
				"IRISManufacturer",
				"FaultCodesReq",
				"ExtendedGuaranteeNoReq",
				"PolicyNoReq",
				"InvoiceNoReq",
				"CircuitRefReq",
				"PartNoReq",
				"OriginalRetailerReq",
				"ReferralNoReq",
				"SerialNoReq",
				"FirmwareReq",
				"WarrantyLabourRateReqOn",
				"SageAccountNo",
				"NoWarrantyInvoices",
				"UseProductCode",
				"UseMSNNo",
				"UseMSNFormat",
                                "MSNFormat",
				"ValidateDateCode",
				"ForceStatusID",
				"POPPeriod",
				"ClaimPeriod",
				"UseQA",
				"UsePreQA",
				"QAExchangeUnits",
				"CompleteOnQAPass",
				"ValidateNetworkQA",
				"ExcludeFromBouncerTable",
				"BouncerPeriod",
				"CreateTechnicalReport",
				"CreateServiceHistoryReport",
				"IncludeOutOfWarrantyJobs",
				"WarrantyAccountNo",
				"WarrantyPeriodFromDOP",
				"TechnicalReportIncludeAdj",
				"PartNoReqForWarrantyAdj",
				"ForceAdjOnNoParts",
				"ForceKeyRepairPart",
				"ForceOutOfWarrantyFaultCodes",
				"WarrantyExchangeFree",
				"UseFaultCodesForOBFJobs",
				"AutoFinalRejectClaimsNotResubmitted",
				"DaysToResubmit",
				"LimitResubmissions",
                                "LimitResubmissionsTo",
				"CopyDOPFromBouncerJob",
				
				"BouncerPeriodBasedOn",
				"DisplaySectionCodes",
				"DisplayTwoConditionCodes",
				"DisplayJobPriceStructure",
				"DisplayTelNos",
				"DisplayPartCost",
				"DisplayRepairTime",
				"DisplayAuthNo",
				"DisplayOtherCosts",
				"DisplayAdditionalFreight",
				"NoOfClaimPrints",
				"EDIClaimNo",
				"EDIClaimFileName",
				"EDIExportFormatID",
                                
        ];

    }
    
   
    
    /**
    * Description
    * 
    * This method is for fetching data from database
    * 
    * @param array $args Its an associative array contains where clause, limit and order etc.
    * @global $this->conn
    * @global $this->table 
    * @global $this->table_network_manufacturer
    * @global $this->dbColumns
    * @return array 
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */  
    
    public function fetch($args) {
        
        $NetworkID   = isset($args['firstArg']) ? $args['firstArg'] : '';
        $showAssigned= isset($args['secondArg']) ? $args['secondArg'] : '';
        $ClientID    = isset($args['thirdArg']) ? $args['thirdArg'] : '';
        
        if($NetworkID != '' || $ClientID!='') 
        {
            if($ClientID != '' && $showAssigned) 
            {
                $dbTablesColumns2= ['T1.ManufacturerID', 'T1.ManufacturerName', 'T1.Acronym', 'T1.Status', array('T1.ManufacturerID', 'T1.ManufacturerID AS Assigned')];
                $dbTables= $this->table . " AS T1 LEFT JOIN client_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID";
                $args['where']= "T2.ClientID='" . $ClientID . "' AND T2.Status='" . $this->controller->statuses[0]['Code'] . "'";
                $output= $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns2, $args);
            }
            else if($NetworkID != '' && $showAssigned && $ClientID=='') 
            {
                $dbTablesColumns2= ['T1.ManufacturerID', 'T1.ManufacturerName', 'T1.Acronym', 'T1.Status', array('T1.ManufacturerID', 'T1.ManufacturerID AS Assigned')];
                $dbTables= $this->table . " AS T1 LEFT JOIN " . $this->table_network_manufacturer . " AS T2 ON T1.ManufacturerID=T2.ManufacturerID";
                $args['where']= "T2.NetworkID='" . $NetworkID . "' AND T2.Status='" . $this->controller->statuses[0]['Code'] . "'";
                $output= $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns2, $args);
            } 
            else 
            {    
                $output= $this->ServeDataTables($this->conn, $this->table, $this->dbTablesColumns, $args);
            }
        } 
        else 
        {
	    $output= $this->ServeDataTables($this->conn, $this->table, $this->dbTableColumns, $args);
        }
       
        return  $output;
        
    }
    
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         if(!isset($args['ManufacturerID']) || !$args['ManufacturerID']) {
	    return $this->create($args);
         } else {
            return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to check whether Manufacturer Name exists.
     *
     * @param interger $ManufacturerID  
     * @param interger $ManufacturerName
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($ManufacturerID, $ManufacturerName) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql= 'SELECT ManufacturerID FROM '.$this->table.' WHERE ManufacturerName=:ManufacturerName AND ManufacturerID!=:ManufacturerID';
        $fetchQuery= $this->conn->prepare($sql, array(PDO::ATTR_CURSOR=> PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ManufacturerID'=> $ManufacturerID, ':ManufacturerName'=> $ManufacturerName));
        $result= $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['ManufacturerID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to check whether network is exists for the manufacturer.
     *
     * @param interger $NetworkID  
     * @param interger $ManufacturerID
     * 
     * @global $this->table_network_manufacturer
     
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isNetworkExists($NetworkID, $ManufacturerID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql= 'SELECT NetworkManufacturerID FROM '.$this->table_network_manufacturer.' WHERE ManufacturerID=:ManufacturerID AND NetworkID=:NetworkID';
        $fetchQuery= $this->conn->prepare($sql, array(PDO::ATTR_CURSOR=> PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ManufacturerID'=> $ManufacturerID, ':NetworkID'=> $NetworkID));
        $result= $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['NetworkManufacturerID'])
        {
                return true;
        }
        
        return false;
    
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to check whether client is exists for the manufacturer.
     *
     * @param interger $ClientID 
     * @param interger $ManufacturerID
     * 
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isClientExists($ClientID, $ManufacturerID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql= 'SELECT ClientManufacturerID FROM client_manufacturer WHERE ManufacturerID=:ManufacturerID AND ClientID=:ClientID';
        $fetchQuery= $this->conn->prepare($sql, array(PDO::ATTR_CURSOR=> PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ManufacturerID'=> $ManufacturerID, ':ClientID'=> $ClientID));
        $result= $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['ClientManufacturerID'])
        {
                return true;
        }
        
        return false;
    
    }
    
    
    
    
    
    
   
    
    
    
    
    
    /**
     * getManufacturerId
     *  
     * Get the ManufacturerID from the name of a manufacturer
     * 
     * @param string $mName    The nameof the manufacturer
     * 
     * @return integer  Manufacturer ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getManufacturerId($mName) {
        $sql= "
                SELECT
			`ManufacturerID`
		FROM
			`manufacturer`
		WHERE
			`ManufacturerName`= '$mName'
               ";
        
        $result= $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['ManufacturerID']);                               /* Manufacturer exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    /**
    Returns manufacturer for provided ID
    2012-01-23 Â© Vic <v.rutkunas@pccsuk.com>
    */
    /* Should have been named fetchRow to match other models */
    public function getManufacturer($manufacturerID) {
	
	$q= "SELECT * FROM manufacturer WHERE ManufacturerID= :manufacturerID";
	$values= ["manufacturerID"=> $manufacturerID];
	$result= $this->query($this->conn, $q, $values);
	return (isset($result[0])) ? $result[0] : false;
	
    }
    
    public function authManufacturersList( $ServiceProviderID=null ) {
	
	$q= "SELECT ManufacturerName, 0 AS Auth, 0 as DataChecked, ManufacturerID FROM manufacturer WHERE Status='Active' order by ManufacturerName";
	$result= $this->query($this->conn, $q);
        
        if ($ServiceProviderID=== null) return $result;

        $model= $this->loadModel('ManufacturerServiceProvider');       
        $auths= $model->getManufacturersByServiceProvider( $ServiceProviderID );
        
        foreach($auths as $auth) {
            foreach($result as &$row) {
                if ($row['ManufacturerID']== $auth['ManufacturerID']) {
                    if ($auth['Authorised']== 'Yes') $row['Auth']= 1;
                    if ($auth['DataChecked']== 'Yes') $row['DataChecked']= 1;
                    break;
                }
            }
        }
        
        return $result;
    }      
    
    /**
    Returns all manufacturers
    2012-01-23 Â© Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getAllManufacturers($id=false) {
        if($id){
            $where=" and ManufacturerID=$id";
        }else{
            $where="";
        }
      
	 $q= "SELECT * FROM manufacturer where Status='Active' $where order by ManufacturerName ASC";
	$result= $this->query($this->conn, $q);
        if($id&&isset($result[0])){return $result[0];}else{return $result;}
    }
    
    
    public function getManufacturerData($networkid=false,$clientID=false,$keys=false,$table='manufacturer',$spid='',$aprove='',$status='Active',$unmodified=false){
        $eSql="";
        $eSel="";
        $eSql2="";
        $eSel2="";
        $eWhere="";
        if($spid!=''){
            $eWhere="and ServiceProviderID=$spid";
        }
        if($networkid){
            $eSql=" left join network_manufacturer nm on nm.ManufacturerID=t1.ManufacturerID and nm.NetworkID=$networkid";
            $eSel=",nm.Status as `asig`";
            
            }
        if($clientID){
            $eSql2=" left join client_manufacturer nc on nc.ManufacturerID=nm.ManufacturerID and nc.ClientID=$clientID";
            $eSel2=",nc.Status as `csig`";
            
            }
            if($table=='manufacturer')
            {
                if($aprove==''){
                    $aprove='Approved';
                }
                $eWhere.=" and ApproveStatus='$aprove'";
            }
            if($unmodified){
                $eWhere.=" and t1.CreatedDate='0000-00-00 00:00:00'";
            }
      
 if(!$keys){
     $q= "SELECT * $eSel $eSel2 FROM $table t1
            $eSql $eSql2 where t1.Status='Active'
            
            ";
 }else{
       
         $q= "SELECT ".implode(",",$keys)." $eSel $eSel2 FROM $table t1
            $eSql $eSql2 where t1.Status='$status'  $eWhere
            
            ";
 }
 
	$result= $this->query($this->conn, $q);
	return $result;
    }
    
    public function saveAssignedManufacturers($p,$u)
     {
      $ModifiedUserID=$u;
        
        if($p['cid']==""){
            $table="network_manufacturer";
             $where="NetworkID=:NetworkID";
             $insert="NetworkID";
             $params=array('NetworkID'=>$p['nid']);
           
             
        }else{
            $table="client_manufacturer";
            $where="ClientID=:ClientID";
            $insert="ClientID";
             $params=array('ClientID'=>$p['cid']);
        }
        $sql="update $table set Status='In-Active' where $where";
       $this->Execute($this->conn, $sql,$params);
        
       for($i=0;$i<sizeof($p['assignedMan']);$i++){
            $manid=$p['assignedMan'][$i];
           $sql="select * from $table where $where and ManufacturerID=$manid";
           $res= $this->query($this->conn, $sql,$params);
           if(isset($res[0])){
               $sql="update $table set Status='Active' where $where and ManufacturerID=$manid;";
           }else{
               $sql="insert into $table (ManufacturerID,$insert,CreatedDate,ModifiedUserID) values ($manid,:$insert,now(),$ModifiedUserID);";
           }
        $this->Execute($this->conn, $sql,$params);
       }
       
       
       
    }
    
    public function saveManufacturer($p,$t1,$t2=false){
       $extraCol="";
         $extraToken="";
         $extraParam=array();
        if($t1=="manufacturer"){
            $idcol="ManufacturerID";
            $PrimarySupplierIDName="PrimarySupplierID";
            $JobFaultCodeIDName="JobFaultCodeID";
            }else{
                $idcol="ServiceProviderManufacturerID";
                $PrimarySupplierIDName="PrimaryServiceProviderSupplierID";
                $JobFaultCodeIDName="ServiceProviderJobFaultCodeID";
                
                }
                
      if(!isset($p['Status'])){$p['Status']="Active";}
      if(!isset($p['AuthorisationRequired'])){$p['AuthorisationRequired']="No";}
      
      
       if($p['mode']=="insert" ||$p['mode']=="copyNew"){
             
             
             if($p['mode']=="copyNew"){
                 $extraCol=",ServiceProviderID,ManufacturerID";
                 $extraToken=",:ServiceProviderID,:ManufacturerID";
                 $extraParam= $params=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "ManufacturerID" =>$p["ManufacturerID"], 
                     );
             }
             
        $sql= "INSERT INTO	$t1 
						(
                                ManufacturerName, 
                                Acronym,
				Status, 
				AuthorisationRequired,
				AuthorisationManager,
				AuthorisationManagerEmail,
                              
				AuthReqChangedDate,
				CreatedDate, 
				ModifiedUserID, 
				ModifiedDate,
                                AccountNo,
                                Postcode,
                                BuildingNameNumber,
				Street,
				LocalArea,
				TownCity,
				CountryID,
				TelNoSwitchboard,
				TelNoSpares,
				TelNoWarranty,
				TelNoTechnical,
				FaxNo,
				EmailAddress,
				WarrantyEmailAddress,
				SparesEmailAddress,
				ContactName,
				$PrimarySupplierIDName,
				StartClaimNo,
				VATNo,
				IRISManufacturer,
				FaultCodesReq,
				ExtendedGuaranteeNoReq,
				PolicyNoReq,
				InvoiceNoReq,
				CircuitRefReq,
				PartNoReq,
				OriginalRetailerReq,
				ReferralNoReq,
				SerialNoReq,
				FirmwareReq,
				WarrantyLabourRateReqOn,
				SageAccountNo,
				NoWarrantyInvoices,
				UseProductCode,
				UseMSNNo,
				UseMSNFormat,
                                MSNFormat,
				ValidateDateCode,
				ForceStatusID,
				POPPeriod,
				ClaimPeriod,
				UseQA,
				UsePreQA,
				QAExchangeUnits,
				CompleteOnQAPass,
				ValidateNetworkQA,
				ExcludeFromBouncerTable,
				BouncerPeriod,
				CreateTechnicalReport,
				CreateServiceHistoryReport,
				IncludeOutOfWarrantyJobs,
				WarrantyAccountNo,
				WarrantyPeriodFromDOP,
				TechnicalReportIncludeAdj,
				PartNoReqForWarrantyAdj,
				ForceAdjOnNoParts,
				ForceKeyRepairPart,
				ForceOutOfWarrantyFaultCodes,
				WarrantyExchangeFree,
				UseFaultCodesForOBFJobs,
				AutoFinalRejectClaimsNotResubmitted,
				DaysToResubmit,
				LimitResubmissions,
                                LimitResubmissionsTo,
				CopyDOPFromBouncerJob,
				$JobFaultCodeIDName,
				
				
				BouncerPeriodBasedOn,
				DisplaySectionCodes,
				DisplayTwoConditionCodes,
				DisplayJobPriceStructure,
				DisplayTelNos,
				DisplayPartCost,
				DisplayRepairTime,
				DisplayAuthNo,
				DisplayOtherCosts,
				DisplayAdditionalFreight,
				NoOfClaimPrints,
				EDIClaimNo,
				EDIClaimFileName,
				EDIExportFormatID
                                $extraCol
                                                        )
				VALUES		(
                                :ManufacturerName,
                                :Acronym,
				:Status, 
				:AuthorisationRequired,
				:AuthorisationManager,
				:AuthorisationManagerEmail,
                              
				NOW(),
				:CreatedDate, 
				:ModifiedUserID, 
				:ModifiedDate,
                                :AccountNo,
                                :PostCode,
                                :BuildingNameNumber,
				:Street,
				:LocalArea,
				:TownCity,
				:CountryID,
				:TelNoSwitchboard,
				:TelNoSpares,
				:TelNoWarranty,
				:TelNoTechnical,
				:FaxNo,
				:EmailAddress,
				:WarrantyEmailAddress,
				:SparesEmailAddress,
				:ContactName,
				:$PrimarySupplierIDName,
				:StartClaimNo,
				:VATNo,
				:IRISManufacturer,
				:FaultCodesReq,
				:ExtendedGuaranteeNoReq,
				:PolicyNoReq,
				:InvoiceNoReq,
				:CircuitRefReq,
				:PartNoReq,
				:OriginalRetailerReq,
				:ReferralNoReq,
				:SerialNoReq,
				:FirmwareReq,
				:WarrantyLabourRateReqOn,
				:SageAccountNo,
				:NoWarrantyInvoices,
				:UseProductCode,
				:UseMSNNo,
				:UseMSNFormat,
                                :MSNFormat,
				:ValidateDateCode,
				:ForceStatusID,
				:POPPeriod,
				:ClaimPeriod,
				:UseQA,
				:UsePreQA,
				:QAExchangeUnits,
				:CompleteOnQAPass,
				:ValidateNetworkQA,
				:ExcludeFromBouncerTable,
				:BouncerPeriod,
				:CreateTechnicalReport,
				:CreateServiceHistoryReport,
				:IncludeOutOfWarrantyJobs,
				:WarrantyAccountNo,
				:WarrantyPeriodFromDOP,
				:TechnicalReportIncludeAdj,
				:PartNoReqForWarrantyAdj,
				:ForceAdjOnNoParts,
				:ForceKeyRepairPart,
				:ForceOutOfWarrantyFaultCodes,
				:WarrantyExchangeFree,
				:UseFaultCodesForOBFJobs,
				:AutoFinalRejectClaimsNotResubmitted,
				:DaysToResubmit,
				:LimitResubmissions,
                                :LimitResubmissionsTo,
				:CopyDOPFromBouncerJob,
				:$JobFaultCodeIDName,
				
				
				:BouncerPeriodBasedOn,
				:DisplaySectionCodes,
				:DisplayTwoConditionCodes,
				:DisplayJobPriceStructure,
				:DisplayTelNos,
				:DisplayPartCost,
				:DisplayRepairTime,
				:DisplayAuthNo,
				:DisplayOtherCosts,
				:DisplayAdditionalFreight,
				:NoOfClaimPrints,
				:EDIClaimNo,
				:EDIClaimFileName,
				:EDIExportFormatID
                                $extraToken                
                                    )";
        $params2=array( 'CreatedDate'=> date("Y-m-d H:i:s"));
       
        
       
        
        }
        
        
        
        
        
          //this is used when new entry is created it must be insertedfirst in main table, then secondary
         if($p['mode']=="New"){
                  
            $idcol="ManufacturerID";
            $PrimarySupplierIDName="PrimarySupplierID";
            $JobFaultCodeIDName="JobFaultCodeID";
            
              $extraCol=",ApproveStatus";
                 $extraToken=",:ApproveStatus";
                 $extraParam= $params=array(
     "ApproveStatus" =>"Pending", 
     $JobFaultCodeIDName=>  $p['JobFaultCodeID'],
                     $PrimarySupplierIDName=>  $p['PrimarySupplierID'],
                     );
                 
                 
           
                
                
                
                
                
                
         $sql= "INSERT INTO	$t2 
						(
                                ManufacturerName, 
                                Acronym,
				Status, 
				AuthorisationRequired,
				AuthorisationManager,
				AuthorisationManagerEmail,
                              
				AuthReqChangedDate,
				CreatedDate, 
				ModifiedUserID, 
				ModifiedDate,
                                AccountNo,
                                Postcode,
                                BuildingNameNumber,
				Street,
				LocalArea,
				TownCity,
				CountryID,
				TelNoSwitchboard,
				TelNoSpares,
				TelNoWarranty,
				TelNoTechnical,
				FaxNo,
				EmailAddress,
				WarrantyEmailAddress,
				SparesEmailAddress,
				ContactName,
				$PrimarySupplierIDName,
				StartClaimNo,
				VATNo,
				IRISManufacturer,
				FaultCodesReq,
				ExtendedGuaranteeNoReq,
				PolicyNoReq,
				InvoiceNoReq,
				CircuitRefReq,
				PartNoReq,
				OriginalRetailerReq,
				ReferralNoReq,
				SerialNoReq,
				FirmwareReq,
				WarrantyLabourRateReqOn,
				SageAccountNo,
				NoWarrantyInvoices,
				UseProductCode,
				UseMSNNo,
				UseMSNFormat,
                                MSNFormat,
				ValidateDateCode,
				ForceStatusID,
				POPPeriod,
				ClaimPeriod,
				UseQA,
				UsePreQA,
				QAExchangeUnits,
				CompleteOnQAPass,
				ValidateNetworkQA,
				ExcludeFromBouncerTable,
				BouncerPeriod,
				CreateTechnicalReport,
				CreateServiceHistoryReport,
				IncludeOutOfWarrantyJobs,
				WarrantyAccountNo,
				WarrantyPeriodFromDOP,
				TechnicalReportIncludeAdj,
				PartNoReqForWarrantyAdj,
				ForceAdjOnNoParts,
				ForceKeyRepairPart,
				ForceOutOfWarrantyFaultCodes,
				WarrantyExchangeFree,
				UseFaultCodesForOBFJobs,
				AutoFinalRejectClaimsNotResubmitted,
				DaysToResubmit,
				LimitResubmissions,
                                LimitResubmissionsTo,
				CopyDOPFromBouncerJob,
				$JobFaultCodeIDName,
				
				
				BouncerPeriodBasedOn,
				DisplaySectionCodes,
				DisplayTwoConditionCodes,
				DisplayJobPriceStructure,
				DisplayTelNos,
				DisplayPartCost,
				DisplayRepairTime,
				DisplayAuthNo,
				DisplayOtherCosts,
				DisplayAdditionalFreight,
				NoOfClaimPrints,
				EDIClaimNo,
				EDIClaimFileName,
				EDIExportFormatID
                                $extraCol
                                                        )
				VALUES		(
                                :ManufacturerName,
                                :Acronym,
				:Status, 
				:AuthorisationRequired,
				:AuthorisationManager,
				:AuthorisationManagerEmail,
                              
				NOW(),
				now(), 
				:ModifiedUserID, 
				:ModifiedDate,
                                :AccountNo,
                                :PostCode,
                                :BuildingNameNumber,
				:Street,
				:LocalArea,
				:TownCity,
				:CountryID,
				:TelNoSwitchboard,
				:TelNoSpares,
				:TelNoWarranty,
				:TelNoTechnical,
				:FaxNo,
				:EmailAddress,
				:WarrantyEmailAddress,
				:SparesEmailAddress,
				:ContactName,
				:$PrimarySupplierIDName,
				:StartClaimNo,
				:VATNo,
				:IRISManufacturer,
				:FaultCodesReq,
				:ExtendedGuaranteeNoReq,
				:PolicyNoReq,
				:InvoiceNoReq,
				:CircuitRefReq,
				:PartNoReq,
				:OriginalRetailerReq,
				:ReferralNoReq,
				:SerialNoReq,
				:FirmwareReq,
				:WarrantyLabourRateReqOn,
				:SageAccountNo,
				:NoWarrantyInvoices,
				:UseProductCode,
				:UseMSNNo,
				:UseMSNFormat,
                                :MSNFormat,
				:ValidateDateCode,
				:ForceStatusID,
				:POPPeriod,
				:ClaimPeriod,
				:UseQA,
				:UsePreQA,
				:QAExchangeUnits,
				:CompleteOnQAPass,
				:ValidateNetworkQA,
				:ExcludeFromBouncerTable,
				:BouncerPeriod,
				:CreateTechnicalReport,
				:CreateServiceHistoryReport,
				:IncludeOutOfWarrantyJobs,
				:WarrantyAccountNo,
				:WarrantyPeriodFromDOP,
				:TechnicalReportIncludeAdj,
				:PartNoReqForWarrantyAdj,
				:ForceAdjOnNoParts,
				:ForceKeyRepairPart,
				:ForceOutOfWarrantyFaultCodes,
				:WarrantyExchangeFree,
				:UseFaultCodesForOBFJobs,
				:AutoFinalRejectClaimsNotResubmitted,
				:DaysToResubmit,
				:LimitResubmissions,
                                :LimitResubmissionsTo,
				:CopyDOPFromBouncerJob,
				:$JobFaultCodeIDName,
				
				
				:BouncerPeriodBasedOn,
				:DisplaySectionCodes,
				:DisplayTwoConditionCodes,
				:DisplayJobPriceStructure,
				:DisplayTelNos,
				:DisplayPartCost,
				:DisplayRepairTime,
				:DisplayAuthNo,
				:DisplayOtherCosts,
				:DisplayAdditionalFreight,
				:NoOfClaimPrints,
				:EDIClaimNo,
				:EDIClaimFileName,
				:EDIExportFormatID
                                $extraToken                
                                    )";
       
                
        
        $params=array(
                                'ManufacturerName'=>$p['ManufacturerName'],
                                'Acronym'=>$p['Acronym'],
				'Status'=>$p['Status'], 
				'AuthorisationRequired'=>$p['AuthorisationRequired'],
				'AuthorisationManager'=>$p['AuthorisationManager'],
				'AuthorisationManagerEmail'=>$p['AuthorisationManagerEmail'],
                               
                                'ModifiedUserID'=> $this->controller->user->UserID,
                                'ModifiedDate'=> date("Y-m-d H:i:s"),
                                'AccountNo'=> $p['AccountNo'],
                                'PostCode'=> $p['PostCode'],
				'BuildingNameNumber'=> $p['BuildingNameNumber'],
				'Street'=>  $p['Street'],
				'LocalArea'=>  $p['LocalArea'],
				'TownCity'=>  $p['TownCity'],
				'CountryID'=>  $p['CountryID'],
				'TelNoSwitchboard'=>  $p['TelNoSwitchboard'],
				'TelNoSpares'=>  $p['TelNoSpares'],
				'TelNoWarranty'=>  $p['TelNoWarranty'],
				'TelNoTechnical'=>  $p['TelNoTechnical'],
				'FaxNo'=>  $p['FaxNo'],
				'EmailAddress'=>  $p['EmailAddress'],
				'WarrantyEmailAddress'=>  $p['WarrantyEmailAddress'],
				'SparesEmailAddress'=>  $p['SparesEmailAddress'],
				'ContactName'=>  $p['ContactName'],
				
				'StartClaimNo'=>  $p['StartClaimNo'],
				'VATNo'=>  $p['VATNo'],
				'IRISManufacturer'=>  (isset($p["IRISManufacturer"]) ? "Yes": "No"),
				'FaultCodesReq'=>  (isset($p["FaultCodesReq"]) ? "Yes": "No"),
				'ExtendedGuaranteeNoReq'=>  (isset($p["ExtendedGuaranteeNoReq"]) ? "Yes": "No"),
				'PolicyNoReq'=>  (isset($p["PolicyNoReq"]) ? "Yes": "No"),
				'InvoiceNoReq'=>  (isset($p["InvoiceNoReq"]) ? "Yes": "No"),
				'CircuitRefReq'=>  (isset($p["CircuitRefReq"]) ? "Yes": "No"),
				'PartNoReq'=>  (isset($p["PartNoReq"]) ? "Yes": "No"),
				'OriginalRetailerReq'=>  (isset($p["OriginalRetailerReq"]) ? "Yes": "No"),
				'ReferralNoReq'=>  (isset($p["ReferralNoReq"]) ? "Yes": "No"),
				'SerialNoReq'=>  (isset($p["SerialNoReq"]) ? "Yes": "No"),
				'FirmwareReq'=>  (isset($p["FirmwareReq"]) ? "Yes": "No"),
				'WarrantyLabourRateReqOn'=>  $p['WarrantyLabourRateReqOn'],
				'SageAccountNo'=>$p['SageAccountNo'],
				'NoWarrantyInvoices'=>  (isset($p["NoWarrantyInvoices"]) ? "Yes": "No"),
				'UseProductCode'=>  (isset($p["UseProductCode"]) ? "Yes": "No"),
				'UseMSNNo'=>  (isset($p["UseMSNNo"]) ? "Yes": "No"),
				'UseMSNFormat'=>  (isset($p["UseMSNFormat"]) ? "Yes": "No"),
                                'MSNFormat'=> $p['MSNFormat'],
				'ValidateDateCode'=>  (isset($p["ValidateDateCode"]) ? "Yes": "No"),
				'ForceStatusID'=>  $p['ForceStatusID'],
				'POPPeriod'=>  $p['POPPeriod'],
				'ClaimPeriod'=>  $p['ClaimPeriod'],
				'UseQA'=>  (isset($p["UseQA"]) ? "Yes": "No"),
				'UsePreQA'=>  (isset($p["UsePreQA"]) ? "Yes": "No"),
				'QAExchangeUnits'=>  (isset($p["QAExchangeUnits"]) ? "Yes": "No"),
				'CompleteOnQAPass'=>  (isset($p["CompleteOnQAPass"]) ? "Yes": "No"),
				'ValidateNetworkQA'=>  (isset($p["ValidateNetworkQA"]) ? "Yes": "No"),
				'ExcludeFromBouncerTable'=>  (isset($p["ExcludeFromBouncerTable"]) ? "Yes": "No"),
				'BouncerPeriod'=>  $p['BouncerPeriod'],
				'CreateTechnicalReport'=>  (isset($p["CreateTechnicalReport"]) ? "Yes": "No"),
				'CreateServiceHistoryReport'=>  (isset($p["CreateServiceHistoryReport"]) ? "Yes": "No"),
				'IncludeOutOfWarrantyJobs'=>  (isset($p["IncludeOutOfWarrantyJobs"]) ? "Yes": "No"),
				'WarrantyAccountNo'=>  $p['WarrantyAccountNo'],
				'WarrantyPeriodFromDOP'=>  $p['WarrantyPeriodFromDOP'],
				'TechnicalReportIncludeAdj'=>  (isset($p["TechnicalReportIncludeAdj"]) ? "Yes": "No"),
				'PartNoReqForWarrantyAdj'=>  (isset($p["PartNoReqForWarrantyAdj"]) ? "Yes": "No"),
				'ForceAdjOnNoParts'=>  (isset($p["ForceAdjOnNoParts"]) ? "Yes": "No"),
				'ForceKeyRepairPart'=>  (isset($p["ForceKeyRepairPart"]) ? "Yes": "No"),
				'ForceOutOfWarrantyFaultCodes'=>  (isset($p["ForceOutOfWarrantyFaultCodes"]) ? "Yes": "No"),
				'WarrantyExchangeFree'=>  $p['WarrantyExchangeFree'],
				'UseFaultCodesForOBFJobs'=>  (isset($p["PolicyNoReq"]) ? "Yes": "No"),
				'AutoFinalRejectClaimsNotResubmitted'=>  (isset($p["AutoFinalRejectClaimsNotResubmitted"]) ? "Yes": "No"),
				'DaysToResubmit'=>  $p['DaysToResubmit'],
				'LimitResubmissions'=>  (isset($p["LimitResubmissions"]) ? "Yes": "No"),
				'LimitResubmissionsTo'=>  $p['LimitResubmissionsTo'],
				'CopyDOPFromBouncerJob'=>  (isset($p["CopyDOPFromBouncerJob"]) ? "Yes": "No"),
				
			
				
				'BouncerPeriodBasedOn'=>  $p['BouncerPeriodBasedOn'],
				'DisplaySectionCodes'=>  (isset($p["DisplaySectionCodes"]) ? "Yes": "No"),
				'DisplayTwoConditionCodes'=>  (isset($p["DisplayTwoConditionCodes"]) ? "Yes": "No"),
				'DisplayJobPriceStructure'=>  (isset($p["DisplayJobPriceStructure"]) ? "Yes": "No"),
				'DisplayTelNos'=>  (isset($p["DisplayTelNos"]) ? "Yes": "No"),
				'DisplayPartCost'=>  (isset($p["DisplayPartCost"]) ? "Yes": "No"),
				'DisplayRepairTime'=>  (isset($p["DisplayRepairTime"]) ? "Yes": "No"),
				'DisplayAuthNo'=>  (isset($p["DisplayAuthNo"]) ? "Yes": "No"),
				'DisplayOtherCosts'=>  (isset($p["DisplayOtherCosts"]) ? "Yes": "No"),
				'DisplayAdditionalFreight'=>  (isset($p["DisplayAdditionalFreight"]) ? "Yes": "No"),
				'NoOfClaimPrints'=>  $p['NoOfClaimPrints'],
				'EDIClaimNo'=>  $p['EDIClaimNo'],
				'EDIClaimFileName'=>  (isset($p["EDIClaimFileName"]) ? $p['EDIClaimFileName']: null),
				'EDIExportFormatID'=>  $p['EDIExportFormatID']
        );
         $params1=  array_merge($params,$extraParam);
            $this->Execute($this->conn, $sql,$params1);  
           $mainID=$this->conn->lastInsertId();   
            $extraCol=",ServiceProviderID,ManufacturerID";
                 $extraToken=",:ServiceProviderID,:ManufacturerID";
                 
                      $PrimarySupplierIDName="PrimaryServiceProviderSupplierID";
                $JobFaultCodeIDName="ServiceProviderJobFaultCodeID";
                 
                 $extraParam=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "ManufacturerID" =>$mainID, 
                     $JobFaultCodeIDName=>  $p['JobFaultCodeID'],
                     $PrimarySupplierIDName=>  $p['PrimarySupplierID'],
                     );
      
                
           
                
                
                
                
          $sql= "INSERT INTO $t1 
						(
                                ManufacturerName, 
                                Acronym,
				Status, 
				AuthorisationRequired,
				AuthorisationManager,
				AuthorisationManagerEmail,
                              AuthReqChangedDate,
				CreatedDate, 
				ModifiedUserID, 
				ModifiedDate,
                                AccountNo,
                                Postcode,
                                BuildingNameNumber,
				Street,
				LocalArea,
				TownCity,
				CountryID,
				TelNoSwitchboard,
				TelNoSpares,
				TelNoWarranty,
				TelNoTechnical,
				FaxNo,
				EmailAddress,
				WarrantyEmailAddress,
				SparesEmailAddress,
				ContactName,
				$PrimarySupplierIDName,
				StartClaimNo,
				VATNo,
				IRISManufacturer,
				FaultCodesReq,
				ExtendedGuaranteeNoReq,
				PolicyNoReq,
				InvoiceNoReq,
				CircuitRefReq,
				PartNoReq,
				OriginalRetailerReq,
				ReferralNoReq,
				SerialNoReq,
				FirmwareReq,
				WarrantyLabourRateReqOn,
				SageAccountNo,
				NoWarrantyInvoices,
				UseProductCode,
				UseMSNNo,
				UseMSNFormat,
                                MSNFormat,
				ValidateDateCode,
				ForceStatusID,
				POPPeriod,
				ClaimPeriod,
				UseQA,
				UsePreQA,
				QAExchangeUnits,
				CompleteOnQAPass,
				ValidateNetworkQA,
				ExcludeFromBouncerTable,
				BouncerPeriod,
				CreateTechnicalReport,
				CreateServiceHistoryReport,
				IncludeOutOfWarrantyJobs,
				WarrantyAccountNo,
				WarrantyPeriodFromDOP,
				TechnicalReportIncludeAdj,
				PartNoReqForWarrantyAdj,
				ForceAdjOnNoParts,
				ForceKeyRepairPart,
				ForceOutOfWarrantyFaultCodes,
				WarrantyExchangeFree,
				UseFaultCodesForOBFJobs,
				AutoFinalRejectClaimsNotResubmitted,
				DaysToResubmit,
				LimitResubmissions,
                                LimitResubmissionsTo,
				CopyDOPFromBouncerJob,
				$JobFaultCodeIDName,
				BouncerPeriodBasedOn,
				DisplaySectionCodes,
				DisplayTwoConditionCodes,
				DisplayJobPriceStructure,
				DisplayTelNos,
				DisplayPartCost,
				DisplayRepairTime,
				DisplayAuthNo,
				DisplayOtherCosts,
				DisplayAdditionalFreight,
				NoOfClaimPrints,
				EDIClaimNo,
				EDIClaimFileName,
				EDIExportFormatID
                                $extraCol
                                                        )
				VALUES		(
                                :ManufacturerName,
                                :Acronym,
				:Status, 
				:AuthorisationRequired,
				:AuthorisationManager,
				:AuthorisationManagerEmail,
                                 NOW(),
				now(), 
				:ModifiedUserID, 
				:ModifiedDate,
                                :AccountNo,
                                :PostCode,
                                :BuildingNameNumber,
				:Street,
				:LocalArea,
				:TownCity,
				:CountryID,
				:TelNoSwitchboard,
				:TelNoSpares,
				:TelNoWarranty,
				:TelNoTechnical,
				:FaxNo,
				:EmailAddress,
				:WarrantyEmailAddress,
				:SparesEmailAddress,
				:ContactName,
				:$PrimarySupplierIDName,
				:StartClaimNo,
				:VATNo,
				:IRISManufacturer,
				:FaultCodesReq,
				:ExtendedGuaranteeNoReq,
				:PolicyNoReq,
				:InvoiceNoReq,
				:CircuitRefReq,
				:PartNoReq,
				:OriginalRetailerReq,
				:ReferralNoReq,
				:SerialNoReq,
				:FirmwareReq,
				:WarrantyLabourRateReqOn,
				:SageAccountNo,
				:NoWarrantyInvoices,
				:UseProductCode,
				:UseMSNNo,
				:UseMSNFormat,
                                :MSNFormat,
				:ValidateDateCode,
				:ForceStatusID,
				:POPPeriod,
				:ClaimPeriod,
				:UseQA,
				:UsePreQA,
				:QAExchangeUnits,
				:CompleteOnQAPass,
				:ValidateNetworkQA,
				:ExcludeFromBouncerTable,
				:BouncerPeriod,
				:CreateTechnicalReport,
				:CreateServiceHistoryReport,
				:IncludeOutOfWarrantyJobs,
				:WarrantyAccountNo,
				:WarrantyPeriodFromDOP,
				:TechnicalReportIncludeAdj,
				:PartNoReqForWarrantyAdj,
				:ForceAdjOnNoParts,
				:ForceKeyRepairPart,
				:ForceOutOfWarrantyFaultCodes,
				:WarrantyExchangeFree,
				:UseFaultCodesForOBFJobs,
				:AutoFinalRejectClaimsNotResubmitted,
				:DaysToResubmit,
				:LimitResubmissions,
                                :LimitResubmissionsTo,
				:CopyDOPFromBouncerJob,
				:$JobFaultCodeIDName,
				:BouncerPeriodBasedOn,
				:DisplaySectionCodes,
				:DisplayTwoConditionCodes,
				:DisplayJobPriceStructure,
				:DisplayTelNos,
				:DisplayPartCost,
				:DisplayRepairTime,
				:DisplayAuthNo,
				:DisplayOtherCosts,
				:DisplayAdditionalFreight,
				:NoOfClaimPrints,
				:EDIClaimNo,
				:EDIClaimFileName,
				:EDIExportFormatID
                                $extraToken                
                                    )";
        
         $params=  array_merge($params,$extraParam);


           $this->Execute($this->conn, $sql,$params);       
         }
        
        
        
        
         if($p['mode']=="update"){
            $id=$p['ManufacturerID'];
            
          if(isset($p['ApproveStatus'])){
              $st=$p['ApproveStatus'];
              $ApproveStatus=",ApproveStatus='$st'";
          }else{
              $ApproveStatus="";
          }
          
          if($p['CreatedDate']=="0000-00-00 00:00:00"){
             $p['CreatedDate']=date('Y-m-d H:i:s');
          }
        
          
            $sql="update $t1 set
                 
                                ManufacturerName=:ManufacturerName, 
                                Acronym=:Acronym, 
				`Status`=:Status,
				 AuthorisationRequired=:AuthorisationRequired,
                                 AuthorisationManager=:AuthorisationManager,
                                AuthorisationManagerEmail=:AuthorisationManagerEmail,
                                ModifiedUserID=:ModifiedUserID, 
				ModifiedDate=:ModifiedDate,
				AccountNo=:AccountNo,
                                Postcode=:PostCode,
				BuildingNameNumber=:BuildingNameNumber,
				Street=:Street,
				LocalArea=:LocalArea,
				TownCity=:TownCity,
				CountryID=:CountryID,
				TelNoSwitchboard=:TelNoSwitchboard,
				TelNoSpares=:TelNoSpares,
				TelNoWarranty=:TelNoWarranty,
				TelNoTechnical=:TelNoTechnical,
				FaxNo=:FaxNo,
				EmailAddress=:EmailAddress,
				WarrantyEmailAddress=:WarrantyEmailAddress,
				SparesEmailAddress=:SparesEmailAddress,
				ContactName=:ContactName,
				$PrimarySupplierIDName=:$PrimarySupplierIDName,
				StartClaimNo=:StartClaimNo,
				VATNo=:VATNo,
				IRISManufacturer=:IRISManufacturer,
				FaultCodesReq=:FaultCodesReq,
				ExtendedGuaranteeNoReq=:ExtendedGuaranteeNoReq,
				PolicyNoReq=:PolicyNoReq,
				InvoiceNoReq=:InvoiceNoReq,
				CircuitRefReq=:CircuitRefReq,
				PartNoReq=:PartNoReq,
				OriginalRetailerReq=:OriginalRetailerReq,
				ReferralNoReq=:ReferralNoReq,
				SerialNoReq=:SerialNoReq,
				FirmwareReq=:FirmwareReq,
				WarrantyLabourRateReqOn=:WarrantyLabourRateReqOn,
				SageAccountNo=:SageAccountNo,
				NoWarrantyInvoices=:NoWarrantyInvoices,
				UseProductCode=:UseProductCode,
				UseMSNNo=:UseMSNNo,
				UseMSNFormat=:UseMSNFormat,
                                MSNFormat=:MSNFormat,
				ValidateDateCode=:ValidateDateCode,
				ForceStatusID=:ForceStatusID,
				POPPeriod=:POPPeriod,
				ClaimPeriod=:ClaimPeriod,
				UseQA=:UseQA,
				UsePreQA=:UsePreQA,
				QAExchangeUnits=:QAExchangeUnits,
				CompleteOnQAPass=:CompleteOnQAPass,
				ValidateNetworkQA=:ValidateNetworkQA,
				ExcludeFromBouncerTable=:ExcludeFromBouncerTable,
				BouncerPeriod=:BouncerPeriod,
				CreateTechnicalReport=:CreateTechnicalReport,
				CreateServiceHistoryReport=:CreateServiceHistoryReport,
				IncludeOutOfWarrantyJobs=:IncludeOutOfWarrantyJobs,
				WarrantyAccountNo=:WarrantyAccountNo,
				WarrantyPeriodFromDOP=:WarrantyPeriodFromDOP,
				TechnicalReportIncludeAdj=:TechnicalReportIncludeAdj,
				PartNoReqForWarrantyAdj=:PartNoReqForWarrantyAdj,
				ForceAdjOnNoParts=:ForceAdjOnNoParts,
				ForceKeyRepairPart=:ForceKeyRepairPart,
				ForceOutOfWarrantyFaultCodes=:ForceOutOfWarrantyFaultCodes,
				WarrantyExchangeFree=:WarrantyExchangeFree,
				UseFaultCodesForOBFJobs=:UseFaultCodesForOBFJobs,
				AutoFinalRejectClaimsNotResubmitted=:AutoFinalRejectClaimsNotResubmitted,
				DaysToResubmit=:DaysToResubmit,
				LimitResubmissions=:LimitResubmissions,
				LimitResubmissionsTo=:LimitResubmissionsTo,
				CopyDOPFromBouncerJob=:CopyDOPFromBouncerJob,
				$JobFaultCodeIDName=:$JobFaultCodeIDName,
			
				
				BouncerPeriodBasedOn=:BouncerPeriodBasedOn,
				DisplaySectionCodes=:DisplaySectionCodes,
				DisplayTwoConditionCodes=:DisplayTwoConditionCodes,
				DisplayJobPriceStructure=:DisplayJobPriceStructure,
				DisplayTelNos=:DisplayTelNos,
				DisplayPartCost=:DisplayPartCost,
				DisplayRepairTime=:DisplayRepairTime,
				DisplayAuthNo=:DisplayAuthNo,
				DisplayOtherCosts=:DisplayOtherCosts,
				DisplayAdditionalFreight=:DisplayAdditionalFreight,
				NoOfClaimPrints=:NoOfClaimPrints,
				EDIClaimNo=:EDIClaimNo,
				EDIClaimFileName=:EDIClaimFileName,
                                CreatedDate=:CreatedDate,
				EDIExportFormatID=:EDIExportFormatID
                               $ApproveStatus
                                where $idcol=$id
            ";
            $params2=array();
            
        }
     
        $params=array(
                                'ManufacturerName'=>$p['ManufacturerName'],
                                'Acronym'=>$p['Acronym'],
				'Status'=>$p['Status'], 
				'AuthorisationRequired'=>$p['AuthorisationRequired'],
				'AuthorisationManager'=>$p['AuthorisationManager'],
				'AuthorisationManagerEmail'=>$p['AuthorisationManagerEmail'],
                               
                                'ModifiedUserID'=> $this->controller->user->UserID,
                                'ModifiedDate'=> date("Y-m-d H:i:s"),
                                'AccountNo'=> $p['AccountNo'],
                                'PostCode'=> $p['PostCode'],
				'BuildingNameNumber'=> $p['BuildingNameNumber'],
				'Street'=>  $p['Street'],
				'LocalArea'=>  $p['LocalArea'],
				'TownCity'=>  $p['TownCity'],
				'CountryID'=>  $p['CountryID'],
				'TelNoSwitchboard'=>  $p['TelNoSwitchboard'],
				'TelNoSpares'=>  $p['TelNoSpares'],
				'TelNoWarranty'=>  $p['TelNoWarranty'],
				'TelNoTechnical'=>  $p['TelNoTechnical'],
				'FaxNo'=>  $p['FaxNo'],
				'EmailAddress'=>  $p['EmailAddress'],
				'WarrantyEmailAddress'=>  $p['WarrantyEmailAddress'],
				'SparesEmailAddress'=>  $p['SparesEmailAddress'],
				'ContactName'=>  $p['ContactName'],
				$PrimarySupplierIDName=>  $p['PrimarySupplierID'],
				'StartClaimNo'=>  $p['StartClaimNo'],
				'VATNo'=>  $p['VATNo'],
				'IRISManufacturer'=>  (isset($p["IRISManufacturer"]) ? "Yes": "No"),
				'FaultCodesReq'=>  (isset($p["FaultCodesReq"]) ? "Yes": "No"),
				'ExtendedGuaranteeNoReq'=>  (isset($p["ExtendedGuaranteeNoReq"]) ? "Yes": "No"),
				'PolicyNoReq'=>  (isset($p["PolicyNoReq"]) ? "Yes": "No"),
				'InvoiceNoReq'=>  (isset($p["InvoiceNoReq"]) ? "Yes": "No"),
				'CircuitRefReq'=>  (isset($p["CircuitRefReq"]) ? "Yes": "No"),
				'PartNoReq'=>  (isset($p["PartNoReq"]) ? "Yes": "No"),
				'OriginalRetailerReq'=>  (isset($p["OriginalRetailerReq"]) ? "Yes": "No"),
				'ReferralNoReq'=>  (isset($p["ReferralNoReq"]) ? "Yes": "No"),
				'SerialNoReq'=>  (isset($p["SerialNoReq"]) ? "Yes": "No"),
				'FirmwareReq'=>  (isset($p["FirmwareReq"]) ? "Yes": "No"),
				'WarrantyLabourRateReqOn'=>  $p['WarrantyLabourRateReqOn'],
				'SageAccountNo'=>$p['SageAccountNo'],
				'NoWarrantyInvoices'=>  (isset($p["NoWarrantyInvoices"]) ? "Yes": "No"),
				'UseProductCode'=>  (isset($p["UseProductCode"]) ? "Yes": "No"),
				'UseMSNNo'=>  (isset($p["UseMSNNo"]) ? "Yes": "No"),
				'UseMSNFormat'=>  (isset($p["UseMSNFormat"]) ? "Yes": "No"),
                                'MSNFormat'=> $p['MSNFormat'],
				'ValidateDateCode'=>  (isset($p["ValidateDateCode"]) ? "Yes": "No"),
				'ForceStatusID'=>  $p['ForceStatusID'],
				'POPPeriod'=>  $p['POPPeriod'],
				'ClaimPeriod'=>  $p['ClaimPeriod'],
				'UseQA'=>  (isset($p["UseQA"]) ? "Yes": "No"),
				'UsePreQA'=>  (isset($p["UsePreQA"]) ? "Yes": "No"),
				'QAExchangeUnits'=>  (isset($p["QAExchangeUnits"]) ? "Yes": "No"),
				'CompleteOnQAPass'=>  (isset($p["CompleteOnQAPass"]) ? "Yes": "No"),
				'ValidateNetworkQA'=>  (isset($p["ValidateNetworkQA"]) ? "Yes": "No"),
				'ExcludeFromBouncerTable'=>  (isset($p["ExcludeFromBouncerTable"]) ? "Yes": "No"),
				'BouncerPeriod'=>  $p['BouncerPeriod'],
				'CreateTechnicalReport'=>  (isset($p["CreateTechnicalReport"]) ? "Yes": "No"),
				'CreateServiceHistoryReport'=>  (isset($p["CreateServiceHistoryReport"]) ? "Yes": "No"),
				'IncludeOutOfWarrantyJobs'=>  (isset($p["IncludeOutOfWarrantyJobs"]) ? "Yes": "No"),
				'WarrantyAccountNo'=>  $p['WarrantyAccountNo'],
				'WarrantyPeriodFromDOP'=>  $p['WarrantyPeriodFromDOP'],
				'TechnicalReportIncludeAdj'=>  (isset($p["TechnicalReportIncludeAdj"]) ? "Yes": "No"),
				'PartNoReqForWarrantyAdj'=>  (isset($p["PartNoReqForWarrantyAdj"]) ? "Yes": "No"),
				'ForceAdjOnNoParts'=>  (isset($p["ForceAdjOnNoParts"]) ? "Yes": "No"),
				'ForceKeyRepairPart'=>  (isset($p["ForceKeyRepairPart"]) ? "Yes": "No"),
				'ForceOutOfWarrantyFaultCodes'=>  (isset($p["ForceOutOfWarrantyFaultCodes"]) ? "Yes": "No"),
				'WarrantyExchangeFree'=>  $p['WarrantyExchangeFree'],
				'UseFaultCodesForOBFJobs'=>  (isset($p["PolicyNoReq"]) ? "Yes": "No"),
				'AutoFinalRejectClaimsNotResubmitted'=>  (isset($p["AutoFinalRejectClaimsNotResubmitted"]) ? "Yes": "No"),
				'DaysToResubmit'=>  $p['DaysToResubmit'],
				'LimitResubmissions'=>  (isset($p["LimitResubmissions"]) ? "Yes": "No"),
				'LimitResubmissionsTo'=>  $p['LimitResubmissionsTo'],
				'CopyDOPFromBouncerJob'=>  (isset($p["CopyDOPFromBouncerJob"]) ? "Yes": "No"),
            
				$JobFaultCodeIDName=>  $p['JobFaultCodeID'],
			
				
				'BouncerPeriodBasedOn'=>  $p['BouncerPeriodBasedOn'],
				'DisplaySectionCodes'=>  (isset($p["DisplaySectionCodes"]) ? "Yes": "No"),
				'DisplayTwoConditionCodes'=>  (isset($p["DisplayTwoConditionCodes"]) ? "Yes": "No"),
				'DisplayJobPriceStructure'=>  (isset($p["DisplayJobPriceStructure"]) ? "Yes": "No"),
				'DisplayTelNos'=>  (isset($p["DisplayTelNos"]) ? "Yes": "No"),
				'DisplayPartCost'=>  (isset($p["DisplayPartCost"]) ? "Yes": "No"),
				'DisplayRepairTime'=>  (isset($p["DisplayRepairTime"]) ? "Yes": "No"),
				'DisplayAuthNo'=>  (isset($p["DisplayAuthNo"]) ? "Yes": "No"),
				'DisplayOtherCosts'=>  (isset($p["DisplayOtherCosts"]) ? "Yes": "No"),
				'DisplayAdditionalFreight'=>  (isset($p["DisplayAdditionalFreight"]) ? "Yes": "No"),
				'NoOfClaimPrints'=>  $p['NoOfClaimPrints'],
				'EDIClaimNo'=>  $p['EDIClaimNo'],
				'EDIClaimFileName'=>  (isset($p["EDIClaimFileName"]) ? $p['EDIClaimFileName']: null),
                                'CreatedDate'=>$p['CreatedDate'],
				'EDIExportFormatID'=>  $p['EDIExportFormatID']
        );
        
      if($p['mode']!="New"){
        
            if(isset($params2)){
           $params=  array_merge($params,$params2);
       }
       if(isset($extraParam)){
           $params=array_merge($params,$extraParam);
       }

     
         $this->execute($this->conn, $sql,$params); 
      }
		 if(isset($p['ManufacturerLogo'])){
                     $sql="update $t1 set ManufacturerLogo=:ManufacturerLogo where $idcol=:$idcol";
                     switch ($p['mode']){
                         case "New":$id=$this->conn->lastInsertId();break;
                         case "update":$id=$p['ManufacturerID'];break;
                         case "copyNew":$id=$this->conn->lastInsertId();break;
                     }
                     $params=array(
                         'ManufacturerLogo'=>$p['ManufacturerLogo'],
                         $idcol=>$id
                         
                     );
                     $this->execute($this->conn, $sql,$params);
                 }   
    }
    
    public function deleteManufacturer($id,$table){
         ($table=="manufacturer")?$idcol="ManufacturerID":$idcol="ServiceProviderManufacturerID";
        $sql="update $table set Status='In-Active' where $idcol=$id";
         $this->execute($this->conn, $sql);
    }
    
    public function getManufacturerNetwork($mID){
       $sql=""; 
    }
    
    public function addAssocManufacturer($p,$c){
        $sql="insert into associated_manufacturers  (ParentManufacturerID,ChildManufacturerID) values ($p,$c)";
        $this->execute($this->conn, $sql);
    }
    public function delAssocManufacturer($p,$c){
        $sql="delete from associated_manufacturers where ParentManufacturerID=$p and ChildManufacturerID =$c";
        $this->execute($this->conn, $sql);
    }
    public function loadAssocManufacturer($p){
        $sql="select am.ChildManufacturerID,ManufacturerName from associated_manufacturers am
                join manufacturer m on m.ManufacturerID=am.ChildManufacturerID

            where ParentManufacturerID=$p";
        return $this->Query($this->conn, $sql);
    }
    
   
    public function getIDFromMain($spid){
      $sql="select ServiceProviderID from service_provider_manufacturer ss where ss.ManufacturerID=$spid";
      $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0]['ServiceProviderID'];
     }else{
         false;
     }
  }
  
   public function getData($id,$table){
      
       ($table=="manufacturer")?$idcol="ManufacturerID":$idcol="ServiceProviderManufacturerID";
       $sql="select * from $table where $idcol=$id";
       $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
   
   
     /**
    Returns all active service provider manufacturers
    2013-04-29 Andris
    */
    
    public function getAllSpManufacturers($spid) {
       
      
	 $q= "SELECT * FROM service_provider_manufacturer where status='Active' and ServiceProviderID=$spid";
	$result= $this->query($this->conn, $q);
       return $result;
    }
     public function getAllAprovedManufacturers() {
       
      
	 $q= "SELECT *,ManufacturerID as `ServiceProviderManufacturerID` FROM manufacturer where status='Active' and ApproveStatus='Approved'";
	$result= $this->query($this->conn, $q);
       return $result;
    }
    
    
    //convert service provider manufacturer id to manufacturer id
    public function getMainManufacturerIDFromSpMan($mid){
     $sql="select ManufacturerID from service_provider_manufacturer where ServiceProviderManufacturerID=$mid";
        
        $result= $this->query($this->conn, $sql);
       return $result[0]['ManufacturerID'];
        
    }
   
   //convert  manufacturer id to service provider manufacturer id
    public function getSpManFromMainManufacturerID($mid){
     $sql="select ServiceProviderManufacturerID from service_provider_manufacturer where ManufacturerID=$mid";
        
        $result= $this->query($this->conn, $sql);
        if(isset($result[0]['ServiceProviderManufacturerID'])){
            return $result[0]['ServiceProviderManufacturerID'];
        }else{
            return '';
        }
       
        
    }
    
    public function CopyFromMain($ids,$spid){
        $fields=$this->comonFields;
        $sql="insert into service_provider_manufacturer (";
        foreach($fields as $c){
            $sql.="$c,";
        }
         $sql.="ServiceProviderID,ManufacturerID,CreatedDate"; 
				
        $sql=trim($sql,",");
        $sql.=") select ";
        foreach($fields as $c){
            $sql.="$c,";
        }
        $sql=trim($sql,",");
        //extra paramtres
        $sql.=" ,'$spid' as ServiceProviderID,ManufacturerID,''";
        $sql.=" from manufacturer where ManufacturerID in (";
        foreach($ids as $c){
            $sql.="$c,";
        }
        $sql=trim($sql,",");
        $sql.=")";
        $this->Execute($this->conn, $sql);
       
    }
    
    
    public function getMainTableDistinctRecords($spid){
        $sql="select count(*) as c from manufacturer where ManufacturerID not in (select ManufacturerID from service_provider_manufacturer where ServiceProviderID=$spid) and ApproveStatus='Approved' and Status='Active'";
        $result= $this->query($this->conn, $sql);
        return $result[0]["c"]; 
    }
    
    //this function counts how many records have been copied from manufacturer table, but have not been modified by user yet
    public function countSPUnmodifiedRecords($spID){
        $sql="select count(*) as con from service_provider_manufacturer spm where CreatedDate='0000-00-00 00:00:00'";
        $result=$this->query($this->conn, $sql);
        return $result[0]['con'];
    }
    
    
    public function getSPManufacturerModels($spid,$mid){
        $sql="select ServiceProviderModelID,ModelNumber from  service_provider_model where ServiceProviderManufacturerID=$mid and ServiceProviderID=$spid";
        return $result=$this->query($this->conn, $sql);
    }
    
    
}
?>
