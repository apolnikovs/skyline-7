<?php

/**
 * APIJobs.class.php
 * 
 * Database access routines for the Jobs API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link 
 * @version    1.39
 * 
 * Changes
 * Date        Version Author                Reason
 * 25/06/2012  1.00    Andrew J. Williams    Initial Version
 * 16/07/2012  1.01    Andrew J. Williams    Issue 22 - API failure because product_location table no longer exists
 * 26/07/2012  1.02    Andrew J. Williams    Issue 40 - Consent field moved to customer table.
 * 08/08/2012  1.03    Andrew J. Williams    Trackerbase VMS Log 32 - Interface with RMA Tracker for job update
 * 23/08/2012  1.04    Andrew J. Williams    Issue 60 - RMASkylineService/PutNewJob not Sending ServiceBaseUnitType 
 * 24/08/2012  1.05    Andrew J. Williams    TrackerBase VMS Log 29 - Alterations to Skyline API to work with ServiceBase
 * 20/09/2012  1.06    Brian Etherington     Fix $sqlUpdate variable name typos
 * 21/09/2012  1.07    Andrew J. Williams    Issue 82 - Mismatch in response codes between RMA tracker and Skyline on PutNewJob response
 * 21/09/2012  1.08    Andrew J. Williams    Issue 83 - PutNewJob failing due to no ServiceCentre number being returned by RMA tracker.
 * 03/10/2012  1.09    Andrew J. Williams    Issue 86 - putNewJob failing on call by Service Base
 * 03/10/2012  1.10    Andrew J. Williams    Issue 87 - PutNewJob should expect SC0003 to indicate a duplicate not SC0002
 * 04/10/2012  1.11    Andrew J. Williams    Issue 88 - ServiceBase.PutNewJob is not sending the correct model number on some new jobs
 * 05/10/2012  1.12    Andrew J. Williams    Issue 89 - ServiceBase.PutNewJob not sending UnitType
 * 09/10/2012  1.13    Andrew J. Williams    Issue 93 - Sales Receipt No. Not Being Passed To Service Base From Skyline
 * 10/10/2012  1.14    Andrew J. Williams    Issue 95 - Dates produced in the API should be in dd/mm/yyyy format
 * 12/10/2012  1.15    Andrew J. Williams    Added rmaPutNewJob
 * 12/10/2012  1.16    Andrew J. Williams    Issue 98 - Servicebase PutNewJob overwriting RMA Tracker Number
 * 16/10/2012  1.17    Brian Etherington     Fix typo in job create - update RMANumber in skydrive with $sqlUpdate not $sql
 * 17/10/2012  1.18    Andrew J. Williams    JobType being incorrectly sent as Type
 * 24/10/2012  1.19    Andrew J. Williams    Issue 106 - RMA Dates requirement changed from yyyy-mm-dd to dd/mm/yyyy
 * 25/10/2012  1.20    Andrew J. Williams    Issue 106 - Revert as request bu N. Wrighting
 * 07/01/2013  1.21    Andrew J. Williams    Trackerbase VMS Log 95 - Always send account number to to RMA Tracker
 * 07/01/2013  1.22    Andrew J. Williams    Trackerbase VMS Log 98 - Add extra fields to comms for Vestel
 * 07/01/2013  1.23    Andrew J. Williams    Trackerbase VMS Log 97 - Add extra fields for Samsung
 * 07/01/2013  1.24    Andrew J. Williams    Trackerbase VMS Log 100 - Send Contact History Notes to RMA Tracker Issue
 * 08/01/2013  1.25    Andrew J. Williams    Issue 173 - API Error: Undefined index: SLNumber : rmaPutJobDetails
 * 21/01/2013  1.26    Andrew J. Williams    Issue 183 - No Collection telephone number and e-mail delivery into ServiceBase
 * 06/02/2013  1.27    Andrew J. Williams    Issue 200 - GetNewJobs API - Table 'skyline_test.Part' doesn't exist
 * 25/03/2013  1.28    Brian Etherington     Corrected SQL for GetNewJobs to return no jobs (if no jobs found) or more than 1 job (if multiple jobs found)
 *                                           Changed CustName to CustStreet & Type to JobType
 * 11/03/2013  1.29    Andrew J. Williams    Trackerbase VMS Log 230 - getNewjobs API changes for Linhill - Date Format and Model Number
 * 11/03/2013  1.30    Andrew J. Williams    Trackerbase VMS Log 168 - Allow Samsung Ancrum hjobs to download to two places
 * 19/04/2013  1.31    Andrew J. Williams    Data Integrity Check - Referesh Process
 * 03/05/2013  1.32    Andrew J. Williams    Issue 349 - Urgent changes for Skyline to RMA
 * 01/05/2013  1.33    Andrew J. Williams    Trackerbase VMS Log 253 - Add in resend feature between Skyline and RMA 
 * 17/05/2013  1.34    Andrew J. Williams    Failed Servicebase Refresh Jobs will list Service Centre
 * 05/06/2013  1.35    Brian Etherington     Neil Wrighting Email: "the date being sent as ETDDate is in the wrong format.  
 *                                           Please can this be changed to the same format as the BookedDate?"
 * 13/06/2013  1.36    Brian Etherington     When sending a job to RMA tracker through rmaPutJobDetails, 
 *                                           change the code so any part which is sent send the SuppliersOrderNo 
 *                                           in the OrderNo field instead of the OrderNo
 * 13/06/2013  1.37    Brian Etherington     We have an issue where Samsung warranty claims are being uploaded 
 *                                           twice because the setting is never cleared. When a job is sent to 
 *                                           RMA (rmaPutJobDetails). If ClaimTransmitStatus in the Job table is 
 *                                           not 0, after uploading the field to RMA, check if 
 *                                           network_service_provider.SamsungDownloadEnabled is 0, 
 *                                           if it is, set the ClaimTransmitStatus on the job back to 0.
 * 17/06/2013  1.38    Brian Etherington     Correct typo in getSamsungDownloadEnabled
 *                                           Correct typo in rmaPutJobDetails
 * 18/06/2013  1.39    Brian Etherington     Change rmaPutJobDetails so that product related unit type
 *                                           takes precedence over model related unit type.
 *****************************************************************************/

require_once('CustomModel.class.php');
include_once ('SkylineRESTClient.class.php');

class APIJobs extends CustomModel {
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        #$this->debug = false;
        
    }
    
    
    /**
     * Description
     * 
     * Skiline API  ServiceBase.PutNewJob
     * API Spec Section 8.1.4.3
     *  
     * When a new job is booked on Skyline it needs to send this job to the 
     * service centre.  The service centre table stores the IP address and port
     * to connect to for each centre.  As soon as a job is booked and signposted
     * (service centre ID has been attached to the job) the job needs to be
     *  transmitted down to the service centre using the service centre's API.
     * 
     * To cover requirements of Trackerbase Log 168 a second paraamter for 
     * secondary service provider Id has been added. This is an optional second 
     * parameter which should only be used if you wish to overwrite the default
     * service provider iwth a secondayr one.
     * 
     * @param $JobID      The ID of the job (SLNo we are interested in)
     * @param $secSpId    Optional secondary service provider  
     * 
     * @return putNewJobs   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function putNewJob($JobNo, $secSpId = null) {
        $sql = "
                SELECT 
			j.`JobID` AS SLNo,
			j.`RMANumber`,
			j.`AgentRefNo` AS `ClientReferenceNumber`,
			IF(mft.`ManufacturerName` = 'Samsung',						-- If manufacturer is samsung
				j.`NetworkRefNo`							-- Output manufacturers reference
			,
				NULL 
			) AS `NetworkReference`,
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo`
			,
				clt.`AccountNumber`
			) AS ClientAccountNo,
			cu.`CompanyName` AS CustCompany,
                        ct.`Title` AS `CustTitle`, 
                        cu.`ContactFirstName` AS `CustForename`, 
                        cu.`ContactLastName` AS `CustSurname`, 
                        cu.`BuildingNameNumber` AS `CustBuildingName`,
                        cu.`Street` AS `CustStreet`,
                        cu.`LocalArea` AS `CustArea`,
                        cu.`TownCity` AS `CustTown`,
                        cuco.`Name` AS `CustCounty`,
                        cuctry.`Name` AS `CustCountry`,                        
                        cu.`PostalCode` AS `CustPostcode`, 
                        cu.`ContactHomePhone` AS `CustPhNo`,
   			IF (ISNULL(cu.`ContactWorkPhoneExt`) OR cu.`ContactWorkPhoneExt` = '',          -- Is extension blank or null
				cu.`ContactWorkPhone`                                                   -- Yes, so just display phone number
			,
				CONCAT(cu.`ContactWorkPhone`,' ext ',cu.`ContactWorkPhoneExt`)          -- No, so display number and extension
			) AS `CustWrkNo`,
                        cu.`ContactMobile` AS `CustMobileNo`,
                        cu.`ContactEmail` AS `CustEmail`,
                        clt.`ClientName` AS `ClientCompanyName`,
			clt.`BuildingNameNumber` AS `ClientBuildingName`,
			clt.`Street` AS `ClientStreet`,
			clt.`LocalArea` AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			cltco.`Name` AS `ClientCounty`,
			cltctry.`Name` AS `ClientCountry`,
			clt.`PostalCode` AS `ClientPostcode`,
			brch.`BranchNumber`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddCompanyName`
			) AS `ColAddCompanyName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddBuildingNameNumber`
			) AS `ColAddBuildingName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddStreet`
			) AS `ColAddStreet`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddLocalArea`
			) AS `ColAddArea`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				j.`ColAddTownCity`
			) AS `ColAddTown`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				caco.`Name`
			) AS `ColAddCounty`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				cactry.`Name`
			) AS `ColAddCountry`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddPostcode`
			) AS `ColAddPostcode`,
                        IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddEmail`
			) AS `ColAddEmail`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				CONCAT(j.`ColAddPhone`,' ',j.`ColAddPhoneExt`)
			) AS `ColAddPhoneNo`,
			j.`OriginalRetailer` AS `OriginalRetailer`,
			j.`RetailerLocation` AS `RetailerLocation`,
			jt.`Type` AS `JobType`,
			st.`ServiceTypeName`,
			st.`Code` AS `ServiceType`,
			IF (ISNULL(mdl.`ModelNumber`),
				j.`ServiceBaseModel`
                        ,
				mdl.`ModelNumber`
			) AS `ModelName`,
			IF (ISNULL(mft.`ManufacturerName`),
				j.`ServiceBaseManufacturer`
                        ,
				mft.`ManufacturerName`
			) AS `Manufacturer`,
			IF (ISNULL(ut.`UnitTypeName`),
				j.`ServiceBaseUnitType`
                        ,
				ut.`UnitTypeName`
			) AS `UnitType`,
                        j.`SerialNo` AS `SerialNumber`,
			DATE_FORMAT(j.`DateOfPurchase`,'%d/%m/%Y') AS `DOP`,
			DATE_FORMAT(j.`DateOfManufacture`,'%d/%m/%Y') AS `DOM`,
			p.`ProductNo` AS `ProductNumber`,
			j.`PolicyNo` AS `PolicyNo`,
			j.`ReportedFault` AS `ReportedFault`,
                        IF (ISNULL(j.`ReceiptNo`),
				j.`Notes`
			,
				CONCAT(j.`Notes`,' \nSales Receipt No : ',j.`ReceiptNo`)
			) AS `Notes`,
			j.`ConditionCode` AS `ConditionCode`,
			j.`SymptomCode` AS `SymptomCode`,
			j.`GuaranteeCode` AS `GuaranteeCode`,
			j.`ProductLocation` AS `ProductLocation`,
			DATE_FORMAT(j.`DateBooked`,'%d/%m/%Y') AS `BookedDate`,
			j.`TimeBooked` AS `BookedTime`,
			DATE_FORMAT(j.`FaultOccurredDate`,'%d/%m/%Y') AS `FaultOccurredDate`,
			DATE_FORMAT(j.`ETDDate`,'%d/%m/%Y') AS `ETDDate`,
			j.`Accessories` AS `Accessories`,
			j.`UnitCondition` AS `UnitCondition`,
			j.`Insurer` AS `Insurer`,
			j.`AuthorisationNo` AS `AuthorisationNo`,
			IF (cu.`DataProtection` = 'YES',								-- Check Data protection field
				'Y'											-- Yes, so return Y
			,
				'N'											-- Otherwise return N
			) AS `DataConsent`,
                        j.`SamsungSubServiceType`,
                        j.`SamsungRefNo`,
                        j.`ConsumerType`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`,
			sp.`CompanyName`
		FROM
			`job` j LEFT JOIN `appointment` appt ON j.`JobID` = appt.`JobID`				-- Appointment might be set for job
                                LEFT JOIN `product` p ON j.`ProductID` = p.`ProductID`                                  -- Product might be set for job
        		        LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`                                    -- model might be set for job
                                LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`                          -- unit type might be set for model
                                LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`               -- manufacturer might be set
                                LEFT JOIN `county` caco ON j.`ColAddCountyID` = caco.`CountyID`
                                LEFT JOIN `country` cactry ON caco.`CountryID` = cactry.`CountryID`
                                LEFT JOIN `network_service_provider` nsp ON (
                                                                                j.`ServiceProviderID` = nsp.`ServiceProviderID`
                                                                             AND 
                                                                                j.`NetworkID` = nsp.`NetworkID`
                                                                            ),
			(`branch` brch LEFT JOIN `county` brchco ON brch.`CountyID` = brchco.`CountyID`)		-- County might not be set for client
				       LEFT JOIN `country` brchctry ON brch.`CountryID` = brchctry.`CountryID`,		-- Country might not be set for client
			(`client` clt LEFT JOIN `county` cltco ON clt.`CountyID` = cltco.`CountyID`)			-- County might not be set for client
				      LEFT JOIN `country` cltctry ON clt.`CountryID` = cltctry.`CountryID`,		-- Country might not be set for client
			((`customer` cu LEFT JOIN `county` cuco ON cu.`CountyID` = cuco.`CountyID`)			-- County might not be set for customer 
					LEFT JOIN `country` cuctry ON cu.`CountryID` = cuctry.`CountryID`)		-- Country might not be set for customer
					LEFT JOIN `customer_title` ct ON cu.`CustomerTitleID` = ct.`CustomerTitleID`,	-- Title might not be set for customer		
			`job_type` jt,
			`service_provider` sp,
			`service_type` st,
			`user` u
		WHERE
			j.`ClientID` = clt.`ClientID`				-- Every job has a client
			AND j.`CustomerID` = cu.`CustomerID`                    -- Every job has a customer
			AND j.`ServiceTypeID` = st.`ServiceTypeID`		-- Every job has a service type	
			  AND st.`JobTypeID` = jt.`JobTypeID`			-- Every service type has a job type
			AND j.`BranchID` = brch.`BranchID`                      -- Every job has a branch
                ";
        
        /*
         * Check if we want to use the secondary service Provider rather than the one in the job
         */
        if (is_null($secSpId )) {                                               /* No link user by Service provider ID in Job */
            $sql .= "
                	AND j.`ServiceProviderID` = sp.`ServiceProviderID`	-- Job is assigned to a Service Provider
                        AND j.`ServiceProviderID` = u.`ServiceProviderID`	-- Service Centre user id
                    ";
        } else {                                                                /* Link Query to supplied secondary iser ID */
             $sql .= "
                        AND u.`ServiceProviderID` = $secSpId                    -- Service Centre user id (link t0 supplied secondary not that in job)
                    ";
        }

        /* Complete Query */
        $sql .= "
                        AND j.`JobID` = $JobNo                                  -- The job in which we are interested
               ";
        
  /*      		AND IF (ISNULL(u.`BranchID`),				-- Check if user is a branch (Sub Agent)
				IF (ISNULL(u.`ServiceProviderID`),			-- BranchID null not a branch so check service provider
					IF (ISNULL(u.`ClientID`),			-- ServiceProvider null not a ServiceProvider so check client (BookingAgent)
						u.`NetworkID` = j.`NetworkID`		-- ClientID is null. User must be a Network
					,
						u.`ClientID` = j.`ClientID`             -- User is a client (Booking Agent)
					)
				,
					u.`ServiceProviderID` = j.`ServiceProviderID`	-- User is a service provider.
				)
			,
				u.`BranchID` = j.`BranchID`				-- User is a Branch (Sub Agent)
			) */
        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log('putNewJob: '.var_export($result,true));
        
        /*
         * Create and Call REST Client
         */
          
        if ( count($result) > 0) {
            $spName = $result[0]['CompanyName'];                                /* Extract out Company Name (need for addition to notes secondary service provider) */
            unset($result[0]['CompanyName']);
                    
            $restClient = new SkylineRESTClient($this->controller);
            
            $output = $restClient->PutNewJob($result[0]);

            if ($this->debug) $this->controller->log('putNewJob: '.var_export($output,true));

            /*
            * Check and process REST output
            */

            if ( $output['response'] === false ) {
                // Server failure
                $this->controller->log('REST Server Error: '.var_export($output,true));
                /* TODO: mail */
                return('REST Server Error');

            } elseif ( $output['response']['ResponseCode'] == "SC0001" ) {
                /* Success */
                
                /* Trackerbase VMS Log 168 - If secondary SP thennwe wnat to put this in notes and not update SC Job Number */
                if ( is_null($secSpId) ) {                                      /* No secondary Service Provider ID set porcede as "normal" and get SCJobNo */
                    $sqlUpdate = "
                                UPDATE 
                                        `job`
                                SET 
                                        `ServiceCentreJobNo` = :SCJobNo,
                                        `DownloadedToSC` = TRUE
                                WHERE
                                        `JobID` = :JobNo
                                ";
                    $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                    $update_params = array( ':JobNo' => $JobNo );
                    if ( isset($output['response']['SCJobNo']) ) {
                        $update_params[':SCJobNo'] = intval($output['response']['SCJobNo']);
                    } else {
                        $update_params[':SCJobNo'] = 'NULL';
                    } /* fi isset $output['response']['SCJobNo'] */
                } else { 
                    if ( isset($output['response']['SCJobNo']) ) {
                        $sqlUpdate = "
                                    UPDATE 
                                            `job`
                                    SET 
                                            `Notes` = :Notes
                                    WHERE
                                            `JobID` = :JobNo
                                    ";
                        $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                        $update_params = array( ':JobNo' => $JobNo );
                        $notes = $result[0]['Notes'];
                        $notes .= "\n$spName job number : {$output['response']['SCJobNo']} ";
                        $update_params[':Notes'] = $notes;
                    } /* fi isset $output['response']['SCJobNo'] */
                } /* fi is_null $secSpId */

                $update->execute( $update_params );

            } elseif ( $output['response']['ResponseCode'] == "SC0003" ) {    
                /* Duplicate */
                $this->controller->log('SLNumber: '.$output['response']['SLNumber'].' SCJobNo: '.$output['response']['SCJobNo'].' is duplicate at '.$result[0]['Site']);

                $sqlUpdate = "
                            UPDATE 
                                    `job`
                            SET 
                                    `DownloadedToSC` = TRUE
                            WHERE
                                    `JobID` = :JobNo
                            ";
                $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $update->execute( array(':JobNo' => $JobNo) );
            } else {
                /* Failue */

                $message = array (
                                'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output['response']['SLNumber'].' SCJobNo: '.$output['response']['SCJobNo'],
                                'API Error code' => $output['response']['ResponseCode'],
                                    'API Error Description' => $output['response']['ResponseDescription'],
                                'info' => $output['info']
                                );

                $this->controller->log('REST Error: '.var_export($message,true));

                /* TODO: mail */
            }
        } else {
            $output = array('response' => array('ResponseCode' => 'SC0002'));
            $this->controller->log('Put New Job - Fulll record can not be returned. Pssibly missing data in Job create');
            return('SC0002');
        }
        
        return($output['response']['ResponseCode']);
    }
    
    
    /**
     * Description
     * 
     * Skiline API  ServiceBase.PutContactHistory
     * API Spec Section 8.1.4.4
     *  
     * When someone inserts a new contact history entry on Skyline we need to 
     * send this note down to the service centre.  After the record has been 
     * inserted lookup the service centre the job is for and call the 
     * PutContactHistory procedure on the service centres API.
     * 
     * @param $ContactHistoryID - ID of the contact history line we are updating
     * 
     * @return getContactHistory   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function putContactHistory($ContactHistoryID) {
        $sql = "
                SELECT
			ch.`JobID` AS SLNumber,
			j.`RMANumber`,
			ch.`ContactDate` AS `Date`,
			ch.`ContactTime` AS `Time`,
			cha.`Action` AS ActionName,
			ch.`Note` AS Note,
			ch.`UserCode` AS `User`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,		-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
		FROM
			`contact_history` ch,
			`contact_history_action` cha,
			`job` j,
			`service_provider` sp,
			`user` u
		WHERE
			ch.`ContactHistoryActionID` = cha.`ContactHistoryActionID`	-- Each contact history has an action
			AND ch.`JobID` = j.`JobID`					-- Contact History relates to a job
			AND j.`ServiceProviderID` = sp.`ServiceProviderID`		-- Job is assigned to a Service Provider
			AND IF (ISNULL(u.`BranchID`),					-- Check if user is a branch (Sub Agent)
				IF (ISNULL(u.`ServiceProviderID`),			-- BranchID null not a branch so check service provider
					IF (ISNULL(u.`ClientID`),			-- ServiceProvider null not a ServiceProvider so check client (BookingAgent)
						u.`NetworkID` = j.`NetworkID`		-- ClientID is null. User must be a Network
					,
						u.`ClientID` = j.`ClientID`             -- User is a client (Booking Agent)
					)
				,
					u.`ServiceProviderID` = j.`ServiceProviderID`	-- User is a service provider.
				)
			,
				u.`BranchID` = j.`BranchID`				-- User is a Branch (Sub Agent)
			)
                        AND ch.`ContactHitoryID` = $ContactHistoryID                    -- Required contact history item
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        /*
         * Create and Call REST Client
         */
        
        $restClient = new SkylineRESTClient($result[0], "put", $this->controller);
        $output = $restClient->run();
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response']['ResponseCode'] == "SC0001" ) {
            /* Success */
            $sqlUpdate = "
                          UPDATE 
				`contact_history`
			  SET 
				`DownloadedToSC` = TRUE
			  WHERE
				`ContactHitoryID` = :ContactHistoryID
                         ";
            $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $update->execute( array(':ContactHistoryID' => $ContactHistoryID) );      
         } else {
            /* Failue */
            
            $message = array (
                               'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output['response']['SLNumber'].' SCJobNo: '.$output['response']['SCJobNo'],
                               'API Error code' => $output['response']['ResponseCode'],
                               'info' => $output['info']
                             );
            
            $this->controller->log($message);
            
            /* TODO: mail */
        }
        
        return($output['response']['ResponseCode']);
    }
    
    
    /**
     * Description
     * 
     * Skyline API  ServiceBase.PutElectronicInvoiceResponse
     * API Spec Section 8.1.4.5
     *  
     * After an electronic invoice has been amended through the Client Electronic 
     * Invoicing page, call the web service on the service centre involved with 
     * the job and send them  the following information using the 
     * PutElectronicInvoiceResponse.
     * 
     * @param $jobID - ID of the job we wish to get the invoice for.
     * 
     * @return putElectronicInvoiceResponse   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function putElectronicInvoiceResponse($userID, $jobID) {
        $sql = "
                SELECT
			j.`JobID` AS `SLNumber`,
			j.`RMANumber`,
			-- AS `MessageType`,
			-- AS `RejectionMessage`,
			j.`ChargeableLabourCost` AS `LabourCost`,
			j.`ChargeableDeliveryCost` AS `DeliveryCost`,
			SUM(p.`UnitCost`) AS `OtherCost`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,		-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
                FROM
			`job` j,
			`part` p,
			`service_provider` sp,
			`user` u
                WHERE
			j.`ServiceProviderID` = sp.`ServiceProviderID`			-- Job is assigned to a Service Provider			
			AND IF (ISNULL(u.`BranchID`),					-- Check if user is a branch (Sub Agent)
				IF (ISNULL(u.`ServiceProviderID`),			-- BranchID null not a branch so check service provider
					IF (ISNULL(u.`ClientID`),			-- ServiceProvider null not a ServiceProvider so check client (BookingAgent)
						u.`NetworkID` = j.`NetworkID`		-- ClientID is null. User must be a Network
					,
						u.`ClientID` = j.`ClientID`             -- User is a client (Booking Agent)
					)
				,
					u.`ServiceProviderID` = j.`ServiceProviderID`	-- User is a service provider.
				)
			,
				u.`BranchID` = j.`BranchID`				-- User is a Branch (Sub Agent)
			)
			AND j.`JobID` = p.`JobID`
			  AND p.`IsOtherCost` = TRUE
			AND j.`JobID` = $jobID
			AND u.`UserID` = '$userID'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        /*
         * Create and Call REST Client
         */
        
        /* TODO: Usename and password */
        $restClient = new SkylineRESTClient($result[0], "put", $this->controller);
        $output = $restClient->run();
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response']['ResponseCode'] == "SC0001" ) {
            /* Success */
            
            /* Mark message as downloaded */
         } else {
            /* Failue */
            
            $message = array (
                               'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output['response']['SLNumber'].' SCJobNo: '.$output['response']['SCJobNo'],
                               'API Error code' => $output['response']['ResponseCode'],
                               'info' => $output['info']
                             );
            
            $this->controller->log($message);
            
            /* TODO: mail */
        }
        
        return($output['response']['ResponseCode']);
    }
    
    /**
     * Skyline API  ServiceBase.PutWarrantyClaimResponse
     * API Spec Section 8.1.4.6
     *  
     * After an electronic invoice has been amended through the Client Electronic 
     * Invoicing page, call the web service on the service centre involved with 
     * the job and send them  the following information using the 
     * PutElectronicInvoiceResponse.
     * 
     * @param $jobID - ID of the job we wish to get the invoice for.
     * 
     * @return putElectronicInvoiceResponse   Associative array containing
     *                                        reposne fom service base.
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function putWarrantyClaimResponse($userID, $jobID) {
        $sql = "
                SELECT
			j.`JobID` AS `SLNumber`,
                        j.`RMANumber`,
			j.`ClaimNumber` AS `ManufacturerClaimNumber`,
			j.`ChargeableLabourCost` AS `LabourCost`,
			j.`ChargeableDeliveryCost` AS `DeliveryCost`,
			SUM(p.`UnitCost`) AS `OtherCost`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,		-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
                FROM
			`job` j,
			`part` p,
			`service_provider` sp,
			`user` u
                WHERE
			j.`ServiceProviderID` = sp.`ServiceProviderID`			-- Job is assigned to a Service Provider			
			AND IF (ISNULL(u.`BranchID`),					-- Check if user is a branch (Sub Agent)
				IF (ISNULL(u.`ServiceProviderID`),			-- BranchID null not a branch so check service provider
					IF (ISNULL(u.`ClientID`),			-- ServiceProvider null not a ServiceProvider so check client (BookingAgent)
						u.`NetworkID` = j.`NetworkID`		-- ClientID is null. User must be a Network
					,
						u.`ClientID` = j.`ClientID`             -- User is a client (Booking Agent)
					)
				,
					u.`ServiceProviderID` = j.`ServiceProviderID`	-- User is a service provider.
				)
			,
				u.`BranchID` = j.`BranchID`				-- User is a Branch (Sub Agent)
			)
			AND j.`JobID` = p.`JobID`
			  AND p.`IsOtherCost` = TRUE
			AND j.`JobID` = $jobID
			AND u.`UserID` = '$userID'
               ";
        
        $result = $this->Query($this->conn, $sql);

        /*
         * Error Codes
         */

        $sqlError = "
                     SELECT
			`ErrorCode`,
			`ErrorDescription`
                    FROM
			`claim_response`
                    WHERE
			`JobID` = $jobID
                    ";
        
        $resultErrors = $this->Query($this->conn, $sqlError);
        
        $result[0]['Errors'] = json_encode($resultErrors);                      /* Parts query encoded a JSON and places in the parts field */
                
        /*
         * Parts 
         */
        
        $sqlParts = "
                     SELECT
			p.`PartNo` AS `PartNumber`,
			p.`UnitCost` AS `PartPrice`
                     FROM
			`part` p
                     WHERE
			p.`JobID` = $jobID
                    ";
        
        $resultParts = $this->Query($this->conn, $sqlParts);
        
        $result[0]['Parts'] = json_encode($resultParts);                        /* Parts query encoded a JSON and places in the parts field */
        
        /*
         * Create and Call REST Client
         */
        
        $restClient = new SkylineRESTClient($result[0], "put", $this->controller);
        $output = $restClient->run();
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response']['ResponseCode'] == "SC0001" ) {
            /* Success */
            
            
         } else {
            /* Failue */
            
            $message = array (
                               'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output['response']['SLNumber'],
                               'API Error code' => $output['response']['ResponseCode'],
                               'info' => $output['info']
                             );
            
            $this->controller->log($message);
            
            /* TODO: mail */
        }
        
        return($output['response']['ResponseCode']);
        
    }
    
    
    /**
     * Description
     * 
     * Skiline API  Skyline.GetNewJobs
     * API Spec Section 8.1.4.9
     *  
     * Get new jobs which have not been marked as downloaded.
     * 
     * @param $UserId     UserID                  
     * 
     * @return getNewJobs   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getNewJobs($UserID) {        
        $sql = "
                SELECT
			j.`JobID` AS SLNumber,
			j.`AgentRefNo` AS `ClientReferenceNumber`,
			IF(mft.`ManufacturerName` = 'Samsung',						-- If manufacturer is samsung
				j.`NetworkRefNo`,							-- Output manufacturers reference
				NULL 
			) AS `NetworkReference`,
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo`
			,
				clt.`AccountNumber`
			) AS ClientAccountNo,
                        ct.`Title` AS `CustTitle`, 
                        cu.`CompanyName` AS `CustCompany`,
                        cu.`ContactFirstName` AS `CustForename`, 
                        cu.`ContactLastName` AS `CustSurname`, 
                        cu.`BuildingNameNumber` AS `CustBuildingName`,
                        cu.`Street` AS `CustStreet`,
                        cu.`LocalArea` AS `CustArea`,
                        cu.`TownCity` AS `CustTown`,
                        cuco.`Name` AS `CustCounty`,
                        cuctry.`Name` AS `CustCountry`,                        
                        cu.`PostalCode` AS `CustPostcode`, 
                        cu.`ContactHomePhone` AS `CustPhNo`,
   			IF (ISNULL(cu.`ContactWorkPhoneExt`) OR cu.`ContactWorkPhoneExt` = '',          -- Is extension blank or null
				cu.`ContactWorkPhone`,                                                   -- Yes, so just display phone number
				CONCAT(cu.`ContactWorkPhone`,' ext ',cu.`ContactWorkPhoneExt`)          -- No, so display number and extension
			) AS `CustWrkNo`,
                        cu.`ContactMobile` AS `CustMobileNo`,
                        cu.`ContactEmail` AS `CustEmail`,
                        clt.`ClientName` AS `ClientCompanyName`,
			clt.`BuildingNameNumber` AS `ClientBuildingName`,
			clt.`Street` AS `ClientStreet`,
			clt.`LocalArea` AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			cltco.`Name` AS `ClientCounty`,
			cltctry.`Name` AS `ClientCountry`,
			clt.`PostalCode` AS `ClientPostcode`,
			brch.`BranchName` AS `BranchCompanyName`,
			brch.`BranchNumber`,
			brch.`BuildingNameNumber` AS `BranchBuildingName`,
			brch.`Street` AS `BranchStreet`,
			brch.`LocalArea` AS `BranchArea`,
			brch.`TownCity` AS `BranchTown`,
			brchco.`Name` AS `BranchCounty`,
			brchctry.`Name` AS `BranchCountry`,
			brch.`PostalCode` AS BranchPostCode,
			j.`OriginalRetailer` AS `OriginalRetailer`,
			j.`RetailerLocation` AS `RetailerLocation`,
			jt.`Type` AS `JobType`,
			st.`ServiceTypeName`,
			st.Code AS `ServiceType`,
			IF (ISNULL(mdl.`ModelNumber`),
				j.`ServiceBaseModel`
                        ,
				mdl.`ModelNumber`
			) AS `ModelName`,
                        j.`SerialNo` AS `SerialNo`,
                        IF (ISNULL(mft.`ManufacturerName`),
				j.`ServiceBaseManufacturer`,
				mft.`ManufacturerName`
			) AS `Manufacturer`,
			IF (ISNULL(ut.`UnitTypeName`),
				j.`ServiceBaseUnitType`,
				ut.`UnitTypeName`
			) AS `UnitType`,
                        j.`SerialNo` AS `SerialNumber`,
			j.`DateOfPurchase` AS `DOP`,
			j.`DateOfManufacture` AS `DOM`,
			j.`PolicyNo` AS `PolicyNo`,
			j.`ReportedFault` AS `ReportedFault`,
			j.`Notes` AS `Notes`,
			j.`ConditionCode` AS `ConditionCode`,
			j.`SymptomCode` AS `SymptomCode`,
			j.`GuaranteeCode` AS `GuaranteeCode`,
			j.`ProductLocation` AS `ProductLocation`,
			j.`DateBooked` AS `BookedDate`, 
			j.`TimeBooked` AS `BookedTime`, 
			j.`FaultOccurredDate` AS `FaultOccurredDate`,
			j.`ETDDate` AS `ETDDate`,
			j.`Accessories` AS `Accessories`,
			j.`UnitCondition` AS `UnitCondition`,
			j.`Insurer` AS `Insurer`,
			j.`AuthorisationNo` AS `AuthorisationNo`,
			IF (cu.`DataProtection` = 'YES',				-- Check Data protection field
				'Y',							-- Yes, so return Y
				'N'							-- Otherwise return N
			) AS `DataConsent`
			
		FROM	`job` j

                        LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`                           -- model might be set for job	
                        LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`     -- manufacturer might be set
                        LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`                   -- unit type might be set for product
                        LEFT JOIN `branch` brch ON j.`BranchID` = brch.`BranchID`
                        LEFT JOIN `county` brchco ON brch.`CountyID` = brchco.`CountyID`	       -- County might not be set for client
                        LEFT JOIN `country` brchctry ON brch.`CountryID` = brchctry.`CountryID`       -- Job may have model set 
                        LEFT JOIN `client` clt ON j.`ClientID` = clt.`ClientID`
                        LEFT JOIN `county` cltco ON clt.`CountyID` = cltco.`CountyID`			-- County might not be set for client
                        LEFT JOIN `country` cltctry ON clt.`CountryID` = cltctry.`CountryID`		-- Country might not be set for client
                        LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                        LEFT JOIN `county` cuco ON cu.`CountyID` = cuco.`CountyID` 			-- County might not be set for customer 
                        LEFT JOIN `country` cuctry ON cu.`CountryID` = cuctry.`CountryID`		-- Country might not be set for customer
                        LEFT JOIN `customer_title` ct ON cu.`CustomerTitleID` = ct.`CustomerTitleID`	-- Title might not be set for customer					
                        LEFT JOIN `service_type` st ON j.`ServiceTypeID` = st.`ServiceTypeID`
                        LEFT JOIN `job_type` jt ON st.`JobTypeID` = jt.`JobTypeID`
                        LEFT JOIN user u ON IF (ISNULL(u.`BranchID`),		-- Check if user is a branch (Sub Agent)
				IF (ISNULL(u.`ServiceProviderID`),			-- BranchID null not a branch so check service provider
					IF (ISNULL(u.`ClientID`),			-- ServiceProvider null not a ServiceProvider so check client (BookingAgent)
						u.`NetworkID` = j.`NetworkID`		-- ClientID is null. User must be a Network
					,
						u.`ClientID` = j.`ClientID`             -- User is a client (Booking Agent)
					)
				,
					u.`ServiceProviderID` = j.`ServiceProviderID`	-- User is a service provider.
				)
			,
				u.`BranchID` = j.`BranchID`				-- User is a Branch (Sub Agent)
			)
                        LEFT JOIN `network_service_provider` nsp ON (
                                                j.`ServiceProviderID` = nsp.`ServiceProviderID`
                                             AND 
                                                j.`NetworkID` = nsp.`NetworkID`
                                            )
		WHERE
	  
			(j.`DownloadedToSC` IS NULL			-- Null indicates not downloaded to SC
			    OR j.`DownloadedToSC` = 0)			-- Or 0 indicates not downloaded to SC			
			AND u.`UserID` = $UserID                        -- Authenticated user id					
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);       
    }
    
    /**
     * Description
     * 
     * Skiline API  Skyline.getWarrantyClaimResopons
     * API Spec Section 8.1.4.9
     *  
     * Get the warranty claim response details for a job
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getContactHistory   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
   
    public function getWarrantyClaimResponse($JobID) {
        $sql = "
                SELECT
			j.`JobID` AS `SLNumber`,
			j.`ClaimBillNumber` AS `ManufacturerClaimNumber`,
			NULL AS `Errors`,
			j.`ChargeableLabourCost` AS `LabourCost`,
			j.`ChargeableDeliveryCost` AS `DeliveryCost`,
                        (
			    SELECT
				SUM(p.`UnitCost`)
			    FROM
				`part` p
			    WHERE
				`IsOtherCost` = 'Y'	
			         AND `JobID` = $JobID
			) AS OtherCost,
			NULL AS `Parts`
		FROM
			`job` j
		WHERE
			j.`JobID` = $JobID 					-- Job Id		
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }

    /**
     * Description
     * 
     * Skiline API  Skyline.getWarrantyClaimResoponseErrors
     * API Spec Section 8.1.4.9
     *  
     * Get the warranty claim response error details for a job
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getContactHistory   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
   
    public function getWarrantyClaimResponseErrors($JobID) {
        $sql = "
                SELECT
			cr.`ErrorCode`,
			cr.`ErrorDescription`,
                        NULL AS `ReferenceCode`
		FROM
			`claim_response` cr
		WHERE
			cr.`JobID` = $JobID 	
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }  
    
    /**
     * Description
     * 
     * Skiline API  Skyline.getWarrantyClaimResoponseErrors
     * API Spec Section 8.1.4.9
     *  
     * Get the warranty claim response details for a job
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getContactHistory   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
   
    public function getWarrantyClaimResponseParts($JobID) {
        $sql = "
                SELECT
			`PartNo` AS `PartNumber`,
			`UnitCost` AS `PartPrice`
		FROM
			`part`
		WHERE
			`JobID` = $JobID 	
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }
    
    /**
     * Description
     * 
     * Skyline API  ServiceBase.PutNewJobsResponse
     * API Spec Section 8.1.4.10
     *  
     * Once the new jobs have downloaded, the service centre will need to upload 
     * a response to let Skyline know the job has been successfully downloaded.
     * Skyline should continue to send the jobs in the GetNewJobs call until the
     * response is sent.
     * 
     * @param $JobID      The ID of the job (SLNo we are interested in)
     *        $ScJobNo    The Service Centre's ID of the job
     * 
     * @return putNewJobsResponse   True if successful false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function putNewJobsResponse($JobNo, $ScJobNo) {
        $sql = "
                UPDATE 
                        `job`
                SET 
                        `ServiceCentreJobNo` = :SCJobNo,
                        `DownloadedToSC` = TRUE
                WHERE
                        `JobID` = :JobNo
                ";
        $update = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $success = $update->execute( array(':SCJobNo' => $ScJobNo, ':JobNo' => $JobNo) );
        
        if ($success) {
            $response = array ('code' => 'OK');
        } else {
            $response = array (
                               'code' => $this->conn->errorInfo[0],
                               'drivercode' => $this->conn->errorInfo[1],
                               'message' => $this->conn->errorInfo[2]
                              );
        }
        
        return($response);
    }
    
    /**
     * Description
     * 
     * Skiline API  Skyline.ClientJobSearch
     * API Spec Section 8.1.4.13
     *  
     * Get client code and account number lookup
     * 
     * @param $UserId     UserID of branch
     *        $where      Search clause                  
     * 
     * @return getClientJobSearch   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getClientJobSearch($UserID, $where) {
        $sql = "
                SELECT
			j.`JobID` AS `SLNumber`,
			DATE_FORMAT(j.`DateBooked`,'%d/%m/%Y') AS `BookedDate`,
			s.`StatusName` AS `Status`,
			cu.`ContactLastName` AS `CustSurname`,
			cu.`PostalCode` AS `Postcode`,
			j.`AgentRefNo` AS `ClientRef`,
			mdl.`ModelNumber` AS `ModelNo`,
			ut.`UnitTypeName` AS `Unittype`,
			sc.`CompanyName` AS `ServiceCentreName`,
			j.`ServiceCentreJobNo` AS `SCJobNo`
		FROM
			`user` u,
			`client_branch` clt_b,
			`customer` cu,
			(((((`job` j LEFT JOIN `appointment` appt ON j.`JobID` = appt.`JobID`)			       		-- Appointment might be set for job
			          LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`)	       		-- Job may have manufacturer set
			          LEFT JOIN `product` p ON j.`ProductID` = p.`ProductID`			       		-- Job may have product set
			                                LEFT JOIN `unit_type` ut ON p.`UnitTypeID` = ut.`UnitTypeID`)  		-- Product may have unit type set
			          LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`)                                		-- Job may have model set
				  LEFT JOIN `service_provider` sc ON j.`ServiceProviderID` = sc.`ServiceProviderID`)		-- Job might be unassigned to service centre
			          LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
		WHERE
			u.`BranchID` = clt_b.`BranchID`					-- Branch users have branch ID (otherwise NULL)
			  AND clt_b.`ClientID` = j.`ClientID`                           -- Branch has clients which have jobs		
			    AND j.`CustomerID` = cu.`CustomerID`			-- Job has customer
			AND u.`UserID` = $UserID					-- Authenticated user id
                        AND $where                                                      -- Where cluase craeted by API
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);

    }
    
    
   
    
    /**
     * Description
     * 
     * Skiline API  Skyline.GetJobDetails
     * API Spec Section 8.1.4.14
     *  
     * Get details for sepcific job
     * 
     * @param $JobId     Job ID                  
     * 
     * @return ContactHistoty   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
   
    public function getJobDetails($JobID, $UserID) {
        $sql = "
                SELECT
			j.`JobID` AS `SLNumber`,
			j.`ServiceCentreJobNo` AS SCJobNo,
			j.`RMANumber`,
			j.`AgentRefNo` AS `ClientReferenceNumber`,
			j.`NetworkRefNo` AS `NetworkReference`,
			ct.`Title` AS `CustomerTitle`, 
                        cu.`ContactFirstName` AS `CustomerForename`, 
                        cu.`ContactLastName` AS `CustomerSurname`, 
                        cu.`BuildingNameNumber` AS `CustomerBuildingName`,
                        cu.`Street` AS `CustomerStreet`,
                        cu.`LocalArea` AS `CustAddressArea`,
                        cu.`TownCity` AS `CustAddressTown`,
                        cuco.`Name` AS `CustomerCounty`,
                        cuctry.`Name` AS `CustomerCountry`,                        
                        cu.`PostalCode` AS `CustomerPostcode`, 
			cu.`ContactHomePhone` AS `CustomerHomeTelNo`,
   			IF (ISNULL(cu.`ContactWorkPhoneExt`) OR cu.`ContactWorkPhoneExt` = '',          -- Is extension blank or null
				cu.`ContactWorkPhone`                                                   -- Yes, so just display phone number
			,
				CONCAT(cu.`ContactWorkPhone`,' ext ',cu.`ContactWorkPhoneExt`)          -- No, so display number and extension
			) AS `CustomerWorkTelNo`,
                        cu.`ContactMobile` AS `CustomerMobileNo`,
                        cu.`ContactEmail` AS `CustomerEmailAddress`,
			clt.`ClientName` AS `ClientCompanyName`,
			CONCAT(clt.`BuildingNameNumber`,' ',clt.`Street`) AS `ClientStreet`,
			clt.`LocalArea` AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			cltco.`Name` AS `ClientCounty`,
			cltctry.`Name` AS `ClientCountry`,
			clt.`PostalCode` AS `ClientPostcode`,
			IF (ISNULL(clt.`ContactPhoneExt`) OR clt.`ContactPhoneExt` = '',          	-- Is extension blank or null
				clt.`ContactPhone`                              		        -- Yes, so just display phone number
			,
				CONCAT(clt.`ContactPhone`,' ext ',clt.`ContactPhoneExt`)         	-- No, so display number and extension
			) AS `ClientPhoneNo`,
			clt.`ContactEmail` AS `ClientEmail`,
			brch.`BranchName` AS `BranchCompanyName`,
			brch.`BuildingNameNumber` AS `BranchBuildingName`,
			brch.`Street` AS `BranchStreet`,
			brch.`LocalArea` AS `BranchArea`,
			brch.`TownCity` AS `BranchTown`,
			brchco.`Name` AS `BranchCounty`,
			brchctry.`Name` AS `BranchCountry`,
			brch.`PostalCode` AS BranchPostCode,
			j.`OriginalRetailer` AS `OriginalRetailer`,
			j.`RetailerLocation` AS `RetailerLocation`,
			sp.`CompanyName` AS `ServiceCentreName`,
			sp.`BuildingNameNumber` AS `ServiceCentreBuildingName`,
			sp.`Street` AS `ServiceCentreStreet`,
			sp.`LocalArea` AS `ServiceCentreArea`,
			sp.`TownCity` AS `ServiceCentreTown`,
			spco.`Name` AS `ServiceCentreCounty`,
			sp.`PostalCode` AS `ServiceCentrePostcode`,
			IF (ISNULL(sp.`ContactPhoneExt`) OR sp.`ContactPhoneExt` = '',          	-- Is extension blank or null
				sp.`ContactPhone`                              		                -- Yes, so just display phone number
			,
				CONCAT(sp.`ContactPhone`,' ext ',sp.`ContactPhoneExt`)          	-- No, so display number and extension
			) AS `ServiceCentrePhoneNo`,
			sp.`ContactEmail` AS `ServiceCentreEmail`,
			jt.`Type` AS `JobType`,
			st.Code AS `ServiceType`,
			j.`RepairType` AS `RepairType`,
			mdl.`ModelName` AS `ModelName`,
			mft.`ManufacturerName` AS `ManufacturerName`,
			ut.`UnitTypeName` AS `UnitType`,
                        j.`SerialNo` AS `SerialNumber`,
			DATE_FORMAT(j.`DateOfPurchase`,'%d/%m/%Y') AS `DOP`,
			j.`PolicyNo` AS `PolicyNo`,
			j.`Insurer` AS `Insurer`,
			j.`ReportedFault` AS `ReportedFault`,
			j.`Notes` AS `Notes`,
			j.`ConditionCode` AS `ConditionCode`,
			j.`SymptomCode` AS `SymptomCode`,
			j.`GuaranteeCode` AS `GuaranteeCode`,
			j.`DefectType` AS `DefectType`,
			j.`ProductLocation` AS `ProductLocation`,
			DATE_FORMAT(j.`FaultOccurredDate`,'%d/%m/%Y') AS `DateFaultOccured`,
			DATE_FORMAT(j.`DateBooked`,'%d/%m/%Y') AS `DateBooked`,
			j.`TimeBooked` AS `TimeBooked`,
			j.`DateUnitReceived` AS `DateUnitReceived`,
			j.`RepairCompleteDate` AS `DateRepairComplete`,
			j.`ClosedDate` AS `ClosedDate`,
			j.`ETDDate` AS `ETDDate`,
			j.`RepairDescription` AS `RepairDescription`,
			s.`StatusName`  AS `Status`,
			j.`CompletionStatus` AS `CompletionStatus`,
			j.`Accessories` AS `Accessories`,
			j.`UnitCondition` AS `UnitCondition`,
			j.`JobSite` AS `JobSite`,
			j.`EngineerCode` AS `EngineerCode`,
			j.`EngineerName` AS `EngineerName`,
			j.`ClaimNumber` AS `ClaimNumber`,
			j.`AuthorisationNo` AS `AuthorisationNumber`,
			IF (cu.`DataProtection` = 'YES',								-- Check Data protection field
				'Y'											-- Yes, so return Y
			,
				'N'											-- Otherwise return N
			) AS `DataConsent`,
			j.`ClaimTransmitStatus`,
			j.`eInvoiceStatus`,
			j.`ChargeableLabourCost` AS `ChargeableLabourCost`,
			j.`ChargeableLabourVATCost` AS `ChargeableLabourVATCost`,
			j.`ChargeablePartsCost` AS `ChargeablePartsCost`,
			j.`ChargeableDeliveryCost` AS `ChargeableDeliveryCost`,
			j.`ChargeableDeliveryVATCost` AS `ChargeableDeliveryVATCost`,
			j.`ChargeableVATCost` AS `ChargeableVATCost`,
			j.`ChargeableSubTotal` AS `ChargeableSubTotal`,
			j.`ChargeableTotalCost` AS `ChargeableTotalCost`,
			j.`AgentChargeableInvoiceNo` AS `AgentChargeableInvoiceNo`,
			j.`AgentChargeableInvoiceDate` AS `AgentChargeableInvoiceDate`
		FROM
			(`branch` brch LEFT JOIN `county` brchco ON brch.`CountyID` = brchco.`CountyID`)			-- County might not be set for client
				       LEFT JOIN `country` brchctry ON brch.`CountryID` = brchctry.`CountryID`,
			(`client` clt LEFT JOIN `county` cltco ON clt.`CountyID` = cltco.`CountyID`)				-- County might not be set for client
				      LEFT JOIN `country` cltctry ON clt.`CountryID` = cltctry.`CountryID`,			-- Country might not be set for client
			`client_branch` clt_b,
			((`customer` cu LEFT JOIN `county` cuco ON cu.`CountyID` = cuco.`CountyID`)				-- County might not be set for customer 
					LEFT JOIN `country` cuctry ON cu.`CountryID` = cuctry.`CountryID`)			-- Country might not be set for customer
					LEFT JOIN `customer_title` ct ON cu.`CustomerTitleID` = ct.`CustomerTitleID`,		-- Title might not be set for customer
			(((((`job` j LEFT JOIN `appointment` appt ON j.`JobID` = appt.`JobID`)					-- Appointment might be set for job
			           LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`)			-- Job may have manufacturer set
			           LEFT JOIN `product` p ON j.`ProductID` = p.`ProductID`					-- Job may have product set
			                                 LEFT JOIN `unit_type` ut ON p.`UnitTypeID` = ut.`UnitTypeID`)  	-- Product may have unit type set
			           LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`)					-- Job may have model
			           LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`		-- Job might be unassigned to service centre
							           LEFT JOIN `county` spco ON sp.`CountyID` = spco.`CountyID`)	-- Service centre may have county
			           LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`,						-- Job may have status set
			`job_type` jt,           
			`service_type` st,
			`user` u
		WHERE	
			u.`BranchID` = brch.`BranchID`					-- Branch users have branch ID 		
			  AND brch.`BranchID` = clt_b.`BranchID`			-- Each branch 
			  AND clt_b.`ClientID` = clt.`ClientID`				-- has client(s)
			  AND clt_b.`ClientID` = j.`ClientID`				-- Branch has clients which have jobs
			    AND j.`CustomerID` = cu.`CustomerID`			-- Every job has a customer
			    AND j.`ServiceTypeID` = st.`ServiceTypeID`			-- Every job has a service type
			      AND st.`JobTypeID` = jt.`JobTypeID`			-- Every Service Type has a job Type
			AND j.`JobID` = $JobID                                          -- ID of job we are interested in
			AND u.`UserID` = $UserID                                        -- ID of autrhenticatde user
                       ";
                
        $result = $this->Query($this->conn, $sql);
        
        return($result);  
    }

    
    /**
     * Description
     * 
     * Skiline API  Skyline.GetNewJobs
     * API Spec Section 8.1.4.9
     * API Spec Section 8.1.4.14
     *  
     * Get contact history for jon which as not been downloaded
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getContactHistory   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
   
    public function getContactHistory($JobID) {
        $sql = "
                SELECT
			ch.`JobID` AS SLNumber,
			ch.`ContactDate` AS `Date`,
			ch.`ContactTime` AS `Time`,
			cha.`Action` AS ActionName,
			ch.`Note` AS Note,
			ch.`UserCode` AS `User`
		FROM
			`contact_history` ch,
			`contact_history_action` cha
		WHERE
			ch.`ContactHistoryActionID` = cha.`ContactHistoryActionID`	-- Each contact history has an action
			AND ch.`JobID` = $JobID 					-- Job Id		
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }
    
    
    /**
      * Description
      * 
      * Skiline API  Skyline.GetJobParts
      * API Spec Section 8.1.4.14
      *  
      * Get parts list for specific job
      * 
      * @param $JobId     Job ID                  
      * 
      * @return getJobParts   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      *************************************************************************/
    
    public function getJobParts($JobID) {
        $sql = "
                SELECT
			p.`PartNo` AS `PartNo`,
			p.`PartDescription` AS `Description`,
			p.`Quantity` AS `Quantity`,
			p.`OrderStatus` AS `PartStatus`,
			p.`SectionCode` AS `SectionCode`,
			p.`DefectCode` AS `DefectCode`,
			p.`RepairCode` AS `RepairCode`,
			p.`CircuitReference` AS `CircuitRef`,
			p.`PartSerialNo` AS `PartSerialNo`,
			p.`OldPartSerialNo` AS `OldPartSerialNo`,
			p.`OrderDate` AS `OrderDate`,
			p.`ReceivedDate` AS `OrderReceivedDate`,
			p.`OrderNo` AS `OrderNo`,
			p.`InvoiceNo` AS `OrderInvoiceNo`,
			p.`UnitCost` AS `UnitCost`,
			p.`SaleCost` AS `SaleCost`,
			p.`VATRate` AS `VATRate`,
			p.`IsOtherCost` AS `IsOtherCost`,
			p.`IsAdjustment` AS `IsAdjustment`,
			p.`IsMajorFault` AS `IsMajorFault`
		FROM
			`part` p
		WHERE
			p.`JobID` = $JobID
               ";
                
        $result = $this->Query($this->conn, $sql);
        
        return($result);  
    }
    
    /**
     * Description
     * 
     * Skyline API  Skyline.GetJobAppointments
     * API Spec Section 8.1.4.14
     *  
     * Get appointments list for a specific
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getJobAppointments   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getJobAppointments($JobID) {
        $sql = "
                SELECT
			a.`AppointmentDate`,
			a.`AppointmentTime`,
			a.`AppointmentType`,
			a.`OutCardLeft`,
			a.`EngineerCode`
		FROM
			`appointment` a
		WHERE
			a.`JobID` = $JobID
                        
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result); 
    }
    
    
    /**
     * Description
     * 
     * Skiline API  Skyline.GetJobStatus
     * API Spec Section 8.1.4.14
     *  
     * Get appointments list for a specific
     * 
     * @param $JobId     Job ID                  
     * 
     * @return getJobStatus   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getJobStatus($JobID) {
        $sql = "
                SELECT
			DATE(sh.`Date`) AS `DateChanged`,
			TIME(sh.`Date`) AS `TimeChanged`,
			(
			  SELECT 
				so.`StatusName`
			  FROM 
				`status_history` sho,
				`status` so
			  WHERE
				sho.`Date` < sh.`Date`
				AND sho.`StatusID` = sh.`StatusID`
				AND s.`JobID` = $JobID
			  LIMIT
				1
                          ORDER BY sho.`Date` DESC
			) AS `OldStatus`,
			s.`StatusName` AS `NewStatus`,
			sh.`UserCode` AS `UserCode`

		FROM
			`status` s,
			`status_history` sh
		WHERE
			sh.`StatusID` = s.`StatusID`
			AND s.`JobID` = $JobID
                        
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result); 
    }
    
    /**
     * Description
     * 
     * Skyline API  Skyline.ConsumerJobSearch
     * API Specification Section 8.1.4.15
     *  
     * Get appointments list for a specific
     * 
     * @param $JobId     Job ID
     * 
     * @return consumerJobSearch   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function consumerJobSearch($UserID, $where) {        
        $sql = "
                SELECT 
			j.`JobID` AS `SLNumber`,
			s.`StatusName`  AS `Status`,
			DATE_FORMAT(j.`DateBooked`,'%d/%m/%Y') AS `BookedDate`,
			MAX(a.`AppointmentDate`) AS `FieldCallDate`,
			sp.`CompanyName` AS `ServiceCentreName`,
			sp.`BuildingNameNumber` AS `ServiceCentreBuildingName`,
			sp.`Street` AS `ServiceCentreStreet`,
			sp.`LocalArea` AS `ServiceCentreArea`,
			sp.`TownCity` AS `ServiceCentreTown`,
			spco.`Name` AS `ServiceCentreCounty`,
			sp.`PostalCode` AS `ServiceCentrePostcode`,
			IF (ISNULL(sp.`ContactPhoneExt`) OR sp.`ContactPhoneExt` = '',          		-- Is extension blank or null
				sp.`ContactPhone`                              		                	-- Yes, so just display phone number
			,
				CONCAT(sp.`ContactPhone`,' ext ',sp.`ContactPhoneExt`)          		-- No, so display number and extension
			) AS `ServiceCentrePhoneNo`,
			cu.`PostalCode` AS `CustomerPostCode`,
			cu.`ContactHomePhone` AS `CustomerHomeTelNo` 
		FROM
			(((`job` j RIGHT JOIN `user` u ON u.`NetworkID` = j.`NetworkID`				-- User joined to job relation is dependent on user
			                             OR u.`ServiceProviderID` = j.`ServiceProviderID`		-- type: hence multiple right join. Assumes only relevant
			                             OR u.`ClientID` = j.`ClientID`				-- column in user filled and others are NULL.
			                             OR u.`BranchID` = j.`BranchID`)				-- Could also be filters with IF ISNULLs on full join in WHERE
			          LEFT JOIN `appointment` a ON j.`JobID` = a.`JobID`)				-- Job may have appointments set 
			          LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`		-- Job might be unassigned to service centre
							           LEFT JOIN `county` spco ON sp.`CountyID` = spco.`CountyID`)	-- Service centre may have county
			          LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`,          
			`customer` cu
		WHERE
                        j.`CustomerID` = cu.`CustomerID`			-- Every job has a customer
			AND u.`UserID` = $UserID                                -- Authenticated user
                        $where                                                  -- User search options
                GROUP BY
			j.`JobID`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        #$this->controller->log($sql);
        
        return($result); 
    }
    
    
    /**
     * RMATracker.PutJobDetails
     * 
     * Skyline API  rmaPutJobDetails
     * API Specification Section 8.1.4.15
     *  
     * Send the job details to RMA
     * 
     * @param $jId     Job ID
     * 
     * @return Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function rmaPutJobDetails($jId) {
        /* BEGIN Trackerbase VMS Log 235 - Add in resend feature between Skyline and RMA - ajw 01/05/2013 */
        /* https://bitbucket.org/pccs/skyline/issue/346/trackerbase-vms-log-253-jobs-table-new */
        $sqlMark = "
                    UPDATE 
                          `job`
                    SET 
                          `RMASendFail` = 1
                    WHERE
                          `JobID` = :JobNo
                   ";
        $mark = $this->conn->prepare($sqlMark, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $mark->execute( array(':JobNo' => $jId) );
        /* END Trackerbase VMS Log 235 - Add in resend feature between Skyline and RMA */
        
        $sql = "
                SELECT 
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo`
			,
				clt.`AccountNumber`
			) AS ClientAccountNo,
                        j.`JobID` AS `SLNumber`,
                        j.`RMANumber` AS `RMANumber`,
                        j.`ServiceCentreJobNo` AS `SCJobNo`,
                        j.`AgentRefNo` AS `AgentRefNo`,
                        j.`NetworkRefNo` AS `NetworkRefNo`,
                        cu.`CompanyName` AS `CustomerCompany`,
                        ct.`Title` AS `CustomerTitle`,
                        cu.`ContactFirstName` AS `CustomerForename`,
                        cu.`ContactLastName` AS `CustomerSurname`,
                        cu.`BuildingNameNumber` AS `CustomerBuildingName`,
                        cu.`Street` AS `CustomerStreet`,
                        cu.`LocalArea` AS `CustomerAddressArea`,
                        cu.`TownCity` AS `CustomerAddressTown`,
                        cty.`Name` AS `CustomerCounty`,
                        cntry.`Name` AS `CustomerCountry`,
                        cu.`PostalCode` AS `CustomerPostcode`,
                        cu.`ContactHomePhone` AS `CustomerHomeTelNo`,
                        cu.`ContactWorkPhone` AS `CustomerWorkTelNo`,
                        cu.`ContactMobile` AS `CustomerMobileNo`,
                        cu.`ContactEmail` AS `CustomerEmailAddress`,
                        j.`OriginalRetailer` AS `OriginalRetailer`,
                        j.`RetailerLocation` AS `RetailerLocation`,
                        st.`Code` AS `ServiceType`,
                        jt.`Type` AS `JobType`,
                        IF (ISNULL(mdl.`ModelNumber`),
				j.`ServiceBaseModel`
                        ,
				mdl.`ModelNumber`
			) AS `ModelNo`,
                        j.`SerialNo` AS `SerialNo`,
                        IF (ISNULL(mft.`ManufacturerName`),
				j.`ServiceBaseManufacturer`
                        ,
				mft.`ManufacturerName`
			) AS `Manufacturer`,
			IF (ISNULL(put.`UnitTypeName`),
				IF (ISNULL(ut.`UnitTypeName`),j.`ServiceBaseUnitType`,ut.`UnitTypeName`)
                        ,
				put.`UnitTypeName`
			) AS `UnitType`,
                        j.`VestelModelCode` AS `VestelModelCode`,
			j.`VestelManufactureDate` AS `VestelManufactureDate`,
                        DATE_FORMAT(j.`DateOfPurchase`,'%Y-%m-%d') AS `DOP`,
                        j.`Notes` AS `Notes`,
                        j.`PolicyNo` AS `PolicyNo`,
                        j.`Insurer` AS `Insurer`,
                        j.`ReportedFault` AS `ReportedFault`,
                        j.`ConditionCode` AS `ConditionCode`,
                        j.`SymptomCode` AS `SymptomCode`,
                        j.`GuaranteeCode` AS `GuaranteeCode`,
                        j.`DefectType` AS `DefectType`,
                        DATE_FORMAT(j.`FaultOccurredDate`,'%Y-%m-%d') AS `DateFaultOccurred`,
                        DATE_FORMAT(j.`DateBooked`,'%Y-%m-%d') AS `DateBooked`,
                        j.`TimeBooked` AS `TimeBooked`,
                        j.`DateUnitReceived` AS `DateUnitReceived`,
                        j.`RepairCompleteDate` AS `DateRepairComplete`,
                        j.`ClosedDate` AS `ClosedDate`,
                        j.`ETDDate` AS `ETDDate`,
                        j.`RepairDescription` AS `RepairDescription`,
                        j.`Status` AS `Status`,
                        j.`AgentStatus` AS `AgentStatus`,
                        j.`CompletionStatus` AS `CompletionStatus`,
                        j.`Accessories` AS `Accessories`,
                        j.`UnitCondition` AS `UnitCondition`,
                        j.`JobSite` AS `JobSite`,
                        j.`EngineerCode` AS `EngineerCode`,
                        j.`EngineerName` AS `EngineerName`,
                        j.`ClaimNumber` AS `ClaimNumber`,
                        j.`AuthorisationNo` AS `AuthorisationNumber`,
                        IF (cu.`DataProtection` = 'YES',								-- Check Data protection field
				'Y'											-- Yes, so return Y
			,
				'N'											-- Otherwise return N
			) AS `DataConsent`,
                        j.`ClaimTransmitStatus` AS `ClaimTransmitStatus`,
                        j.`eInvoiceStatus` AS `eInvoiceStatus`,
                        j.`ChargeableLabourCost` AS `ChargeableLabourCost`,
                        j.`ChargeableLabourVATCost` AS `ChargeableLabourVATCost`,
                        j.`ChargeablePartsCost` AS `ChargeablePartsCost`,
                        j.`ChargeableDeliveryCost` AS `ChargeableDeliveryCost`,
                        j.`ChargeableDeliveryVATCost` AS `ChargeableDeliveryVATCost`,
                        j.`ChargeableVATCost` AS `ChargeableVATCost`,
                        j.`ChargeableSubTotal` AS `ChargeableSubTotal`,
                        j.`ChargeableTotalCost` AS `ChargeableTotalCost`,
                        j.`AgentChargeableInvoiceNo` AS `AgentChargeableInvoiceNo`,
                        j.`AgentChargeableInvoiceDate` AS `AgentChargeableInvoiceDate`,
                        CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddCompanyName`
			) AS `ColAddCompanyName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddBuildingNameNumber`
			) AS `ColAddBuildingName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddStreet`
			) AS `ColAddStreet`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddLocalArea`
			) AS `ColAddArea`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				j.`ColAddTownCity`
			) AS `ColAddTown`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				caco.`Name`
			) AS `ColAddCounty`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				cactry.`Name`
			) AS `ColAddCountry`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddPostcode`
			) AS `ColAddPostcode`,
                        IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddEmail`
			) AS `ColAddEmail`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				CONCAT(j.`ColAddPhone`,' ',j.`ColAddPhoneExt`)
			) AS `ColAddPhoneNo`,
                        j.`SamsungSubServiceType`,
                        j.`SamsungRefNo`,
                        j.`ConsumerType`,
                        u.`Username` AS `username`,
			u.`Password` AS `password`
		FROM
			`client` clt,
			((`customer` cu LEFT JOIN `customer_title` ct ON cu.`CustomerTitleID` = ct.`CustomerTitleID`)
			                LEFT JOIN `county` cty ON cu.`CountyID` = cty.`CountyID`)
			                LEFT JOIN `country` cntry ON cu.`CountryID` = cntry.`CountryID`,
			((`job` j LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`)
				  LEFT JOIN `product` p ON j.`ProductID` = p.`ProductID`)					-- Job may have product set
                                                        LEFT JOIN `unit_type` put ON p.`UnitTypeID` = put.`UnitTypeID`
				  LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID` 
			                                LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`
                                  LEFT JOIN `county` caco ON j.`ColAddCountyID` = caco.`CountyID`
                                  LEFT JOIN `country` cactry ON caco.`CountryID` = cactry.`CountryID`
                                  LEFT JOIN `network_service_provider` nsp ON (
										   j.`ServiceProviderID` = nsp.`ServiceProviderID`
                                                                               AND 
                                                                                   j.`NetworkID` = nsp.`NetworkID`
                                                                               ),
			`job_type` jt,			                                
                        `service_type` st,
			`user` u,
                        `service_provider` sp
		WHERE
			j.`ClientID` = clt.`ClientID`
			AND j.`CustomerID` = cu.`CustomerID`
			AND j.`ServiceTypeID` = st.`ServiceTypeID`
			AND st.`JobTypeID` = jt.`JobTypeID`			-- Every service type has a job type
			AND j.`ServiceProviderID` = u.`ServiceProviderID`	-- Service Centre user id
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`	-- Job is assigned to a Service Provider
			AND j.`JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if(count($result) == 0) {                                               /* If no records then exit */
            $response = array(
                              'response' => "rmaPutJobDetails: No results found (or no service cente assigned) for {$jId}",
                              'code' => "SC0003"
                             );
            $this->controller->log($response['response']);
            
            return($response);
        }
        
        $sqlParts = "
                SELECT
			p.`PartNo` AS `PartNo`,
			p.`PartDescription` AS `Description`,
			p.`Quantity` AS `Quantity`,
			p.`OrderStatus` AS `PartStatus`,
			p.`SectionCode` AS `SectionCode`,
			p.`DefectCode` AS `DefectCode`,
			p.`RepairCode` AS `RepairCode`,
			p.`CircuitReference` AS `CircuitRef`,
			p.`PartSerialNo` AS `PartSerialNo`,
			p.`OldPartSerialNo` AS `OldPartSerialNo`,
			p.`OrderDate` AS `OrderDate`,
			p.`ReceivedDate` AS `OrderReceivedDate`,
			p.`SupplierOrderNo` AS `OrderNo`,
			p.`InvoiceNo` AS `OrderInvoiceNo`,
			p.`UnitCost` AS `UnitCost`,
			p.`SaleCost` AS `SaleCost`,
			p.`VATRate` AS `VATRate`,
			p.`IsOtherCost` AS `IsOtherCost`,
			p.`IsAdjustment` AS `IsAdjustment`
		FROM
			`part` p
		WHERE
			p.`JobID` = $jId
               ";
        
        $result[0]['Parts'] = json_encode($this->Query($this->conn, $sqlParts),JSON_HEX_AMP);
        
        $sqlAppointments = "
                SELECT
			DATE_FORMAT(a.`AppointmentDate`,'%Y-%m-%d') AS `AppointmentDate`,
			a.`AppointmentTime`,
			a.`AppointmentType`,
			a.`OutCardLeft`,
			a.`EngineerCode`
		FROM
			`appointment` a
		WHERE
			a.`JobID` = $jId
                ";
        
        $result[0]['Appointments'] = json_encode($this->Query($this->conn, $sqlAppointments),JSON_HEX_AMP );
        
        $sqlStatus = "
               SELECT
			DATE_FORMAT(sh.`Date`,'%Y-%m-%d') AS `DateChanged`,
			TIME(sh.`Date`) AS `TimeChanged`,
			(
			  SELECT 
				so.`StatusName`
			  FROM 
				`status_history` sho,
				`status` so
			  WHERE
				sho.`Date` < sh.`Date`
				AND sho.`StatusID` = sh.`StatusID`
				AND sho.`JobID` = $jId
			  LIMIT
				1
			) AS `OldStatus`,
			s.`StatusName` AS `NewStatus`,
			sh.`UserCode` AS `UserCode`

		FROM
			`status` s,
			`status_history` sh
		WHERE
			sh.`StatusID` = s.`StatusID`
			AND sh.`JobID` = $jId
                     ";
        
        $result[0]['StatusUpdate'] = json_encode($this->Query($this->conn, $sqlStatus),JSON_HEX_AMP);
        
        $sqlCallHist = "
                SELECT
			DATE_FORMAT(ch.`ContactDate`,'%Y-%m-%d') AS `Date`,
			ch.`ContactTime` AS `Time`,
			cha.`Action` AS ActionName,
			ch.`Note` AS Note,
			ch.`UserCode` AS `User`,
			IF (ch.`IsDownloadedToSC` = 0,
				'Y'
			,
				'N'
			) AS `DownloadToSB`
		FROM
			`contact_history` ch,
			`contact_history_action` cha
		WHERE
			ch.`ContactHistoryActionID` = cha.`ContactHistoryActionID`	-- Each contact history has an action
                        AND ch.`Private` = 'N'
			AND ch.`JobID` = $jId            
                      ";
        
        $result[0]['CallHist'] = json_encode($this->Query($this->conn, $sqlCallHist)); 
        
        //$result['Site'] = $this->controller->config['RMATracker']['SamsungApiPath'];
        //$result['Path'] = $this->controller->config['RMATracker']['RmaApiPath'].$this->controller->config['RMATracker']['putJobDetails'];
        $result[0]['Site'] = $this->controller->config['RMATracker']['RmaServer'];
        
        $restClient = new SkylineRESTClient($this->controller);
        $output = $restClient->putJobDetails($result[0]);
        
        if ($this->debug) $this->controller->log(var_export($output,true),JSON_HEX_AMP);
        
        /*
         * Check and process REST output
         */
        
        if ( $output['response'] === false ) {
            // Server failure
            $this->controller->log('REST Server Error: '.var_export($output,true));
            /* TODO: mail */
            return('REST Server Error');
            
        } elseif ( isset($output['response']['ResponseCode']) && $output['response']['ResponseCode'] == "SC0001" ) {
            /* Success */
            
            $samsungDownloadEnabled = $this->getSamsungDownloadEnabled($jId);
            if  ($samsungDownloadEnabled == 0) {
                $sqlUpdate =  "UPDATE 
				`job`
                               SET 
				`RMANumber` = :RMANumber,
                                `RMASendFail` = 0,
                                `ClaimTransmitStatus` = 0
			       WHERE
				`JobID` = :JobNo";
            } else {
                $sqlUpdate =  "UPDATE 
				`job`
                               SET 
				`RMANumber` = :RMANumber,
                                `RMASendFail` = 0
			       WHERE
				`JobID` = :JobNo";
            }

            $update = $this->conn->prepare($sqlUpdate, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $update->execute( array(':RMANumber' => $output['response']['RMANumber'], ':JobNo' => $jId) );
        
        } else {
            /* Failue */
            
            if (! isset($output['response']['ResponseCode'])) $output['response']['ResponseCode'] = '';
            if (! isset($output['response']['ResponseDescription'])) $output['response']['ResponseDescription'] = '';
            
            $message = array (
                               'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$jId,
                               'API Error code' => $output['response']['ResponseCode'],
                               'API Error Description' => $output['response']['ResponseDescription'],
                               'info' => $output['info']
                             );
            
            $this->controller->log('REST Error: '.var_export($message,true));
            
            /* TODO: mail */
        }
        
        return($output['response']['ResponseCode']);
    }
    
    /**
     * Description
     * 
     * Skiline API  rmaPutNewJob
     * API Spec Section 8.1.4.3
     *  
     * When a new job is booked on Skyline it needs to send this job to RMA
     * 
     * @param $JobID      The ID of the job (SLNo we are interested in)
     * 
     * @return  Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function rmaPutNewJob($JobNo) {
        /* BEGIN Trackerbase VMS Log 235 - Add in resend feature between Skyline and RMA - ajw 01/05/2013 */
        /* https://bitbucket.org/pccs/skyline/issue/346/trackerbase-vms-log-253-jobs-table-new */
        $sqlMark = "
                    UPDATE 
                          `job`
                    SET 
                          `RMASendFail` = 1
                    WHERE
                          `JobID` = :JobNo
                   ";
        $mark = $this->conn->prepare($sqlMark, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $mark->execute( array(':JobNo' => $JobNo) );
        /* END Trackerbase VMS Log 235 - Add in resend feature between Skyline and RMA */
        
        $sql = "
                SELECT 
			j.`JobID` AS SLNo,
			j.`RMANumber`,
			j.`AgentRefNo` AS `ClientReferenceNumber`,
			IF(mft.`ManufacturerName` = 'Samsung',						-- If manufacturer is samsung
				j.`NetworkRefNo`							-- Output manufacturers reference
			,
				NULL 
			) AS `NetworkReference`,
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo`
			,
				clt.`AccountNumber`
			) AS ClientAccountNo,
			cu.`CompanyName` AS CustCompany,
                        ct.`Title` AS `CustTitle`, 
                        cu.`ContactFirstName` AS `CustForename`, 
                        cu.`ContactLastName` AS `CustSurname`, 
                        cu.`BuildingNameNumber` AS `CustBuildingName`,
                        cu.`Street` AS `CustStreet`,
                        cu.`LocalArea` AS `CustArea`,
                        cu.`TownCity` AS `CustTown`,
                        cuco.`Name` AS `CustCounty`,
                        cuctry.`Name` AS `CustCountry`,                        
                        cu.`PostalCode` AS `CustPostcode`, 
                        cu.`ContactHomePhone` AS `CustPhNo`,
   			IF (ISNULL(cu.`ContactWorkPhoneExt`) OR cu.`ContactWorkPhoneExt` = '',          -- Is extension blank or null
				cu.`ContactWorkPhone`                                                   -- Yes, so just display phone number
			,
				CONCAT(cu.`ContactWorkPhone`,' ext ',cu.`ContactWorkPhoneExt`)          -- No, so display number and extension
			) AS `CustWrkNo`,
                        cu.`ContactMobile` AS `CustMobileNo`,
                        cu.`ContactEmail` AS `CustEmail`,
                        clt.`ClientName` AS `ClientCompanyName`,
			clt.`BuildingNameNumber` AS `ClientBuildingName`,
			clt.`Street` AS `ClientStreet`,
			clt.`LocalArea` AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			cltco.`Name` AS `ClientCounty`,
			cltctry.`Name` AS `ClientCountry`,
			clt.`PostalCode` AS `ClientPostcode`,
			brch.`BranchNumber`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddCompanyName`
			) AS `ColAddCompanyName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddBuildingNameNumber`
			) AS `ColAddBuildingName`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddStreet`
			) AS `ColAddStreet`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddLocalArea`
			) AS `ColAddArea`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				j.`ColAddTownCity`
			) AS `ColAddTown`,
			IF (j.`ProductLocation` = 'Customer',	
				NULL
			,
				caco.`Name`
			) AS `ColAddCounty`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				cactry.`Name`
			) AS `ColAddCountry`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddPostcode`
			) AS `ColAddPostcode`,
                        IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				j.`ColAddEmail`
			) AS `ColAddEmail`,
			IF (j.`ProductLocation` = 'Customer',
				NULL
			,
				CONCAT(j.`ColAddPhone`,' ',j.`ColAddPhoneExt`)
			) AS `ColAddPhoneNo`,
			j.`OriginalRetailer` AS `OriginalRetailer`,
			j.`RetailerLocation` AS `RetailerLocation`,
			jt.`Type`,
			st.`ServiceTypeName`,
			st.`Code` AS `ServiceType`,
			IF (ISNULL(mdl.`ModelNumber`),
				j.`ServiceBaseModel`
                        ,
				mdl.`ModelNumber`
			) AS `ModelName`,
			IF (ISNULL(mft.`ManufacturerName`),
				j.`ServiceBaseManufacturer`
                        ,
				mft.`ManufacturerName`
			) AS `Manufacturer`,
			IF (ISNULL(ut.`UnitTypeName`),
				j.`ServiceBaseUnitType`
                        ,
				ut.`UnitTypeName`
			) AS `UnitType`,
                        j.`SerialNo` AS `SerialNumber`,
			DATE_FORMAT(j.`DateOfPurchase`,'%Y-%m-%d') AS `DOP`,
			DATE_FORMAT(j.`DateOfManufacture`,'%Y-%m-%d') AS `DOM`,
			p.`ProductNo` AS `ProductNumber`,
			j.`PolicyNo` AS `PolicyNo`,
			j.`ReportedFault` AS `ReportedFault`,
                        IF (ISNULL(j.`ReceiptNo`),
				j.`Notes`
			,
				CONCAT(j.`Notes`,' \nSales Receipt No : ',j.`ReceiptNo`)
			) AS `Notes`,
			j.`ConditionCode` AS `ConditionCode`,
			j.`SymptomCode` AS `SymptomCode`,
			j.`GuaranteeCode` AS `GuaranteeCode`,
			j.`ProductLocation` AS `ProductLocation`,
			DATE_FORMAT(j.`DateBooked`,'%Y-%m-%d') AS `BookedDate`,
			j.`TimeBooked` AS `BookedTime`,
			DATE_FORMAT(j.`FaultOccurredDate`,'%Y-%m-%d') AS `FaultOccurredDate`,
			j.`ETDDate` AS `ETDDate`,
			j.`Accessories` AS `Accessories`,
			j.`UnitCondition` AS `UnitCondition`,
			j.`Insurer` AS `Insurer`,
			j.`AuthorisationNo` AS `AuthorisationNo`,
                        j.`SamsungSubServiceType`,
                        j.`SamsungRefNo`,
                        j.`ConsumerType`,
			IF (cu.`DataProtection` = 'YES',								-- Check Data protection field
				'Y'											-- Yes, so return Y
			,
				'N'											-- Otherwise return N
			) AS `DataConsent`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,						-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
		FROM
			`job` j LEFT JOIN `appointment` appt ON j.`JobID` = appt.`JobID`				-- Appointment might be set for job
                                LEFT JOIN `product` p ON j.`ProductID` = p.`ProductID`                                  -- Product might be set for job
        		        LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`                                    -- model might be set for job
                                LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`                          -- unit type might be set for model
                                LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`               -- manufacturer might be set
                                LEFT JOIN `county` caco ON j.`ColAddCountyID` = caco.`CountyID`
                                LEFT JOIN `country` cactry ON caco.`CountryID` = cactry.`CountryID`
                                LEFT JOIN `network_service_provider` nsp ON (
                                                                                j.`ServiceProviderID` = nsp.`ServiceProviderID`
                                                                             AND 
                                                                                j.`NetworkID` = nsp.`NetworkID`
                                                                            ),
			(`branch` brch LEFT JOIN `county` brchco ON brch.`CountyID` = brchco.`CountyID`)		-- County might not be set for client
				       LEFT JOIN `country` brchctry ON brch.`CountryID` = brchctry.`CountryID`,		-- Country might not be set for client
			(`client` clt LEFT JOIN `county` cltco ON clt.`CountyID` = cltco.`CountyID`)			-- County might not be set for client
				      LEFT JOIN `country` cltctry ON clt.`CountryID` = cltctry.`CountryID`,		-- Country might not be set for client
			((`customer` cu LEFT JOIN `county` cuco ON cu.`CountyID` = cuco.`CountyID`)			-- County might not be set for customer 
					LEFT JOIN `country` cuctry ON cu.`CountryID` = cuctry.`CountryID`)		-- Country might not be set for customer
					LEFT JOIN `customer_title` ct ON cu.`CustomerTitleID` = ct.`CustomerTitleID`,	-- Title might not be set for customer		
			`job_type` jt,
			`service_provider` sp,
			`service_type` st,
			`user` u
		WHERE
			j.`ClientID` = clt.`ClientID`				-- Every job has a client
			AND j.`CustomerID` = cu.`CustomerID`                    -- Every job has a customer
			AND j.`ServiceTypeID` = st.`ServiceTypeID`		-- Every job has a service type	
			  AND st.`JobTypeID` = jt.`JobTypeID`			-- Every service type has a job type
			AND j.`BranchID` = brch.`BranchID`                      -- Every job has a branch
			AND j.`ServiceProviderID` = sp.`ServiceProviderID`	-- Job is assigned to a Service Provider
                        AND j.`ServiceProviderID` = u.`ServiceProviderID`	-- Service Centre user id
                        AND j.`JobID` = $JobNo                                  -- The job in which we are interested
               ";

        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log('rmaPutNewJob: '.var_export($result,true));
        
        /*
         * Create and Call REST Client
         */
          
        if ( count($result) > 0) {
            
            $result[0]['Site'] = $this->controller->config['RMATracker']['RmaServer'];
        
            $restClient = new SkylineRESTClient($this->controller);
            $output = $restClient->putNewJob($result[0]);

            if ($this->debug) $this->controller->log('rmaPutNewJob: '.var_export($output,true));

            /*
            * Check and process REST output
            */

            if ( $output['response'] === false ) {
                // Server failure
                $this->controller->log('REST Server Error: '.var_export($output,true));
                /* TODO: mail */
                return('REST Server Error');

            } elseif ( $output['response']['ResponseCode'] == "SC0001" ) {
                /* Success */
                
                if ( isset($output['response']['SCJobNo']) ) {
                    $scJobNo = intval($output['response']['SCJobNo']);
                } else {
                    $scJobNo = 'NULL';
                }
                if ( isset($output['response']['RMANumber']) ) {
                    $rmano = intval($output['response']['RMANumber']);
                } else {
                    $rmano = 'NULL';
                }
                
                $sqlUpdate = "
                            UPDATE 
                                    `job`
                            SET 
                                    `RMANumber` = $rmano,
                                    `RMASendFail` = 0
                            WHERE
                                    `JobID` = $JobNo
                            ";
                if ($this->debug) $this->controller->log("sqlUpdate = /n ".var_export($sqlUpdate,true));
                if ($this->Execute($this->conn, $sqlUpdate)) {
                    if ($this->debug) $this->controller->log('Successfully updateed');
                } else {
                    $this->controller->log("sqlUpdateError -  /n".$this->lastPDOError());          
                }
            } else {
                /* Failue */

                $message = array (
                                'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output['response']['SLNumber'].' SCJobNo: '.$output['response']['SCJobNo'],
                                'API Error code' => $output['response']['ResponseCode'],
                                    'API Error Description' => $output['response']['ResponseDescription'],
                                'info' => $output['info']
                                );

                $this->controller->log('REST Error: '.var_export($message,true));

                /* TODO: mail */
            }
        } else {
            $output = array('response' => array('ResponseCode' => 'SC0002'));
            $this->controller->log('Put New Job - Fulll record can not be returned. Pssibly missing data in Job create');
            return('SC0002');
        }
        
        return($output['response']['ResponseCode']);
    }
    
    /**
     * skylineRefresh
     * 
     * Skyline API - skylineRefresh
     *  
     * Ask Servicebase to refresh a job on Skyline
     * 
     * @param $jID      The JobID (Skyline job number) of the jon we are to referesh
     * 
     * @return  Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function skylineRefresh($jId) {
        $job_model = $this->controller->loadModel('Job');
        $scName = $job_model->getServiceCentreName($jId);
        $sql = "
                SELECT 
                	j.`ServiceCentreJobNo` AS `SBJobNo`,
			j.`JobID` AS `SLJobNo`,
			CONCAT(sp.`IPAddress`, ':', sp.`Port`) AS `Site`,	-- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
		FROM
			`job` j LEFT JOIN `network_service_provider` nsp ON (
                                                                                j.`ServiceProviderID` = nsp.`ServiceProviderID`
                                                                             AND 
                                                                                j.`NetworkID` = nsp.`NetworkID`
                                                                            ),
			`service_provider` sp,
			`user` u
		WHERE
			j.`ServiceProviderID` = sp.`ServiceProviderID`	-- Job is assigned to a Service Provider
                        AND j.`ServiceProviderID` = u.`ServiceProviderID`	-- Service Centre user id
                        AND j.`JobID` = $jId                                    -- The job in which we are interested
               ";

        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log('skylineRefresh: '.var_export($result,true));
        
        /*
         * Create and Call REST Client
         */
          
        if ( count($result) > 0) {
            /* Success respose */
            
            $restClient = new SkylineRESTClient($this->controller);
            $output = $restClient->sbSkylineRefresh($result[0]);
            
        } else {
            /* Failure response */
            $output = array ('response' => 'FAIL');
        } /* fi count $result  > 0 */
        
        if (trim($output['response']) == '<Response>OK</Response>') {
            //$dlg = "ServiceBase Job Number {$result[0]['SBJobNo']} refresh request registered";
            $dlg = " / SB Job No: {$result[0]['SBJobNo']} has been refreshed - ".$scName;
        } else {
            //$dlg = 'FAIL : '.$job_model->getServiceCentreName($jId);
            $dlg = ' has failed to refresh - '.$scName;
            if ($this->debug) $this->controller->log($output,'servicebase_refresh_');
        }
        
        return($dlg);
    }
    
    private function getSamsungDownloadEnabled($jobID) {
        
        $sql = "select nsp.SamsungDownloadEnabled 
                from network_service_provider nsp
                left join job on job.NetworkID = nsp.NetworkID and job.ServiceProviderID = nsp.ServiceProviderID
                where job.JobID = :JobID";
        $params= array('JobID' => $jobID);
        $result = $this->Query($this->conn, $sql, $params);
        if (count($result) == 0) {
            $this->controller->log('REST Server Error: '.__METHOD__.' empty result set for JobID = '.$jobID);
            $this->controller->log($sql);
            return 0;
        }
        return $result[0]['SamsungDownloadEnabled'];
    }

}

?>
