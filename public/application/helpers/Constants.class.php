<?php

/*******************************************************************
 * Short Description of Constants.
 * 
 * Long description of Constants.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 *  
 * Changes
 * Date        Version Author                Reason
 * 09/10.2013  1.0     Brian Etherington     Initial Version
 ********************************************************************/

class Constants {
    
    const SKYLINE_BRAND_ID = 1000;
    
    // Data Table actions
    const NONE = 0;
    const UPDATE = 1;
    const CREATE = 2;
    const DELETE = 3;
    const VIEW = 4;

    // user Types

    const SuperAdmin = 'Admin';
    const ServiceProvider = 'Service Provider';
    const Manufacturer = 'Manufacturer';
    const ExtendedWarrantor = 'Extended Warrantor';
    const Branch = 'Branch';
    const Client = 'Client';
    const Network = 'Network';
    const UnknownUser = 'Unknown';


}
